let carrousel = document.querySelector(".carrousel");

/* quand on scroll on scroll horizontalement */
carrousel.addEventListener("wheel", (event) => {
    event.preventDefault();
	carrousel.scrollBy({
		left: event.deltaY,
		behavior: 'instant'
	});
});

// fonction pour déterminer la largeur du premier élément du carrousel
function determinerLargeurPremierElement(){
	let premierElementDucarrousel = document.querySelector(".element-carrousel");
	let largeurPremierElementDucarrousel = premierElementDucarrousel.offsetWidth + 15;
	return largeurPremierElementDucarrousel;
}


// fonction pour faire défiler le carrousel
function faireDefilerCarrousel(bouton){
	let largeurPremierElementDucarrousel = determinerLargeurPremierElement();

	let defilement = bouton.id == "flecheGauche" ? -largeurPremierElementDucarrousel : largeurPremierElementDucarrousel;

	if(carrousel.scrollWidth - Math.round(carrousel.scrollLeft) === carrousel.clientWidth && defilement > 0){ // on arrondit scrollLeft car il peut être décimal
		carrousel.scrollTo({
			left: 0,
			behavior: 'smooth'
		});
	} else if(carrousel.scrollLeft == 0 && defilement < 0){
		carrousel.scrollTo({
			left: carrousel.scrollWidth,
			behavior: 'smooth'
		});
	} else{
		carrousel.scrollBy({
			left: defilement,
			behavior: 'smooth'
		});
	}	
}


let boutonsFleches = document.querySelectorAll(".conteneur-carrousel button");

/* quand on clique sur une flèche on scroll */
boutonsFleches.forEach( bouton => {
	bouton.addEventListener("click", () => faireDefilerCarrousel(bouton));
});

function redirigerVersPageInformationsDeLaFormation(nomFormation){
	document.location.href = './informations.php?formation=' + encodeURIComponent(nomFormation.toLowerCase());
}