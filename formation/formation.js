recupererContacts(document.querySelector("#barreContact select")); // on récupère les contacts du département sélectionné

async function recupererContacts(selecteur){
	let corpRequete = new FormData();
	corpRequete.append("numeroDepartement", selecteur.options[selecteur.selectedIndex].value); // on va envoyé au script le département que l'on veut

	const res = await fetch("./dashboard/scriptContactRechercher.php", {
		method: "POST",
		body: corpRequete
	});

	if(res.ok){ // si la réponse est ok
		const donnees = await res.json();

		/* on remplit les champs copiables (numero + mail + adresse physique) */
		if(donnees.numero != ""){
			document.querySelector("#barreContact .affichageNumero").innerHTML = donnees.numero;
		} else{
			document.querySelector("#barreContact .affichageNumero").innerHTML = "Indisponible";
		}

		if(donnees.mail != ""){
			document.querySelector("#barreContact .affichageMail").innerHTML = donnees.mail;
		} else{
			document.querySelector("#barreContact .affichageMail").innerHTML = "Indisponible";
		}

		if(donnees.adresse != ""){
			document.querySelector("#barreContact .affichageAdresse").innerHTML = donnees.adresse;
		} else{
			document.querySelector("#barreContact .affichageAdresse").innerHTML = "Indisponible";
		}

	} else{
		throw new Error("Erreur avec le script de recherche du numéro de téléphone.");
	}
}

/* fonction qui copie dans le clipboard de l'utilisateur */
function copierDansLeClipBoard(numero){
	if(numero != "Indisponible"){
		navigator.clipboard.writeText(numero).then( () => {
			afficherNotification("Copié", "ok");
		}, () => {
			afficherNotification("Erreur dans la copie", "pasOk");
		});
	} else{
		afficherNotification("Cette information n'est pas disponible", "pasOk");
	}
}

/* fonction qui automatise l'affichage des notifications */
function afficherNotification(message, type){
	let notification = document.getElementById("notification");
	notification.innerHTML = message;

	/* cette méthode pour rejouer une animation css vient de mdn web docs */
	notification.removeAttribute("class");
	window.requestAnimationFrame( () => {
		window.requestAnimationFrame( () => {
			notification.setAttribute("class", "afficher" + " " + type);
		});
	});

	/* PS : le code ci-dessous fonctionne également, mais c'est plus une astuce du fait que la méthode offsetWidth actualise passivement (c'est pas sa fonction première mais une conséquence) l'élément du dom sur lequel on l'appelle */
	/*notification.removeAttribute("class");
	notification.offsetWidth;
	notification.setAttribute("class", "afficher" + " " + type);*/
}
