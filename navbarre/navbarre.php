<!-- la barre de navigation -->
<nav id="navbarre" class="avecIcone">

	<!-- le logo -->
	<a id="nav-icone" href="https://www.justicerestaurative.org/antenne-ifjr-sud-ouest/">
		<img src="./assets/nav-icone.png" alt="icone de navigation">
		<p>bienvenue sur portail des formations</p>
	</a>

	<!-- l'hamburger -->
	<button id="hamburger" type="button" onclick="hamburger(this)">
		<span></span>
		<span></span>
		<span></span>
	</button>

	<!-- la boite contenant les liens -->
	<div id="nav-liens">
		<a id="bouton-quitter" href="https://www.justicerestaurative.org/antenne-ifjr-sud-ouest/">quitter</a>
		<a href="formation.php">nos formations</a>
		<a href="informationsEssentielles.php">informations Essentielles</a>
		<a href="listeSessions.php">liste des sessions</a>
		<a href="carte.php">carte intéractive</a>
		<?php
			session_start();
			if (isset($_SESSION["identifiant"])){
				echo('<a href="./dashboard/dashboard.php" style="color: #0aA79B;">interface admin</a>');
				echo('<a href="./connexion/deconnexion.php" style="color: red;">déconnexion</a>');
			} else{
				echo('<a href="connexion.php">connexion</a>');
			}
		?>
	</div>

</nav>

<script type="text/javascript">

	/* quand on scroll la page */
	window.onscroll = () => {
		let positionFenetre = window.scrollY;
		let navebarre = document.getElementById("navbarre");
		let navebarreStatut = navebarre.className;

		if(positionFenetre < 10 && navebarreStatut == "sansIcone"){ // si on est quasiment toute en haut on affiche la barre de navigation entière avec le logo
			navebarre.className = "avecIcone";
		} else if(positionFenetre >= 10 && navebarreStatut == "avecIcone"){ // sinon on cache le logo
			navebarre.className = "sansIcone";
		}
	}

	let navLiens = document.getElementById("nav-liens");

	function hamburger(bouton){ // fonction qui déclenche (ou enlève) l'hamburger
		if(bouton.className != "ouvert"){
			bouton.className = "ouvert";
			navLiens.className = "ouvert";
		} else{
			bouton.removeAttribute("class");
			navLiens.removeAttribute("class");
		}
	}
</script>