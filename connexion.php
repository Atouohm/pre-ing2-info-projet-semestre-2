<?php
	session_start();
	if(isset($_SESSION["identifiant"])){
		header('Location: formation.php');
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="./styleGlobal.css">
	<link rel="stylesheet" href="./navbarre/navbarre.css">
	<link rel="stylesheet" href="./footer/footer.css">
	<link rel="stylesheet" href="./connexion/connexion.css">
	<title>Connexion - Formation</title>
</head>
<body>

	<?php
		include("./navbarre/navbarre.php");
	?>

	<main>

		<!-- formulaure de connexion -->
		<form method="POST" action="./connexion/verificationConnexion.php">
			<label>Identifiant :
				<input name="identifiant" type="text" required placeholder="Identifiant...">
			</label>
			<label>Mot de Passe :
				<input name="motDePasse" type="password" required placeholder="Mot de Passe...">
			</label>
			<?php
				if($_GET["erreur"]=="vrai"){ // si on vient sur cette page avec une erreur de connexion on génère un p pour le dire
					echo('<p id="erreurConnexion">Erreur de connexion !</p>');
				}
			?>
			<button type="submit" name="validerFormationCreer">Valider</button>
		</form>

	</main>

	<?php
		include("./footer/footer.html");
	?>

</body>
</html>
