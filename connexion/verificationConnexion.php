<?php

$ok = false;

if(($fichier = fopen("../proteger/connexion/identifiants.csv", "r")) != false){ // on vérifie que le fichier contenant les identifiants existe
	while((($ligne = fgetcsv($fichier, 999, ";")) != false) && !$ok){ // on parcourt tous les identifiants
		if($_POST['identifiant'] == $ligne[0] && $_POST['motDePasse'] == $ligne[1]){ // si les notres correspondent à au moins 1 couple du fichier c'est ok on connecte l'utilisateur
			$ok = true;
			session_start();
			$_SESSION["identifiant"] = $_POST['identifiant'];
			$_SESSION["motDePasse"] = $_POST['motDePasse'];
		}
	}
	fclose($fichier);
}

if($ok){
	header('Location: ../formation.php');
	exit();
} else{
	header('Location: ../connexion.php?erreur=vrai');
	exit();
}

?>
