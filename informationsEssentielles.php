<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="./styleGlobal.css">
	<link rel="stylesheet" href="./navbarre/navbarre.css">
	<link rel="stylesheet" href="./footer/footer.css">
	<link rel="stylesheet" href="./informationsEssentielles/informationsEssentielles.css">
	<title>Informations Essentielles - Formation</title>
</head>
<body>

	<?php
		include("./navbarre/navbarre.php");
	?>

	<main>


		<?php
			$deroulant = '
				<div class="deroulant">
					<div class="enTete">
						<div class="fleche"><span></span><span></span></div>
						<h3>%s</h3>
					</div>
					<div class="contenu">
						<pre>%s</pre>
					</div>
				</div>
			'; // un déroulant html prêt à être générer avec printf

			if(file_exists("./donnees/informationsEssentielles/informationsEssentielles.json")){
				$contenuFichier = file_get_contents("./donnees/informationsEssentielles/informationsEssentielles.json");
				$donneesFichier = json_decode($contenuFichier, true);

				/* on a vérifier que le fichier des informations essentielles existait, maintenant on regarde s'il n'est pas vide */
				if(count($donneesFichier) != 0){
					foreach($donneesFichier as $clef => $valeur){ // pour toutes les valeurs

						if(preg_match('/^i[0-9]+Titre/', $clef) != null){ // si c'est un titre/une question
							sscanf($clef, 'i%d+Titre', $indiceInformation); // on récupère son indice
							printf($deroulant, $donneesFichier[$clef], $donneesFichier["i".$indiceInformation."Description"]); // on génère le déroulant avec le titre et la description
						}

					}
				}
			} else{
				echo("erreur");
			}
		?>

	</main>

	<?php
		include("./footer/footer.html");
	?>

	<script type="text/javascript" src="./informationsEssentielles/informationsEssentielles.js"></script>
</body>
</html>
