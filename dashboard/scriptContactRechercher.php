<?php
	if(!isset($_POST['numeroDepartement'])){ // on vérifie qu'on a les informations nécessaires pour la suite du script
		http_response_code(400); // sinon on dit bad request
		exit("erreur");
	}

	/* on récupère les données des téléphones + des mails + des adresses */

	$contenuFichierTelephones = file_get_contents("../donnees/contacts/telephones.json");
	$contenuFichierMails = file_get_contents("../donnees/contacts/mails.json");
	$contenuFichierAdresses = file_get_contents("../donnees/contacts/adresses.json");

	$tableauContenuFichierTelephones = json_decode($contenuFichierTelephones, true);
	$tableauContenuFichierMails = json_decode($contenuFichierMails, true);
	$tableauContenuFichierAdresses = json_decode($contenuFichierAdresses, true);

	/* on récupère uniquement les données du département souhaité */

	$tableauDEnvoi = array();
	$tableauDEnvoi += ["numero" => $tableauContenuFichierTelephones[$_POST['numeroDepartement']]];
	$tableauDEnvoi += ["mail" => $tableauContenuFichierMails[$_POST['numeroDepartement']]];
	$tableauDEnvoi += ["adresse" => $tableauContenuFichierAdresses[$_POST['numeroDepartement']]];

	echo(json_encode($tableauDEnvoi)); // on envoie ce qu'on a trouvé
?>
