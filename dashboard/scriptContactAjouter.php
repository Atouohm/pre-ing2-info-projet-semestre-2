<?php
	/* on vérifie que tout va bien */
	session_start();
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] === 'POST') || !isset($_POST['validerContactAjouter'])){
		session_destroy();
		header('Location: ../formation.php');
		exit();
	}

	if(file_exists("../donnees/contacts/telephones.json") &&  file_exists("../donnees/contacts/mails.json") && file_exists("../donnees/contacts/adresses.json")){ // si les fichiers existent bien

		/* on récupère les données */

		$contenuFichierTelephones = file_get_contents("../donnees/contacts/telephones.json");
		$contenuFichierMails = file_get_contents("../donnees/contacts/mails.json");
		$contenuFichierAdresses = file_get_contents("../donnees/contacts/adresses.json");

		$tableauContenuFichierTelephones = json_decode($contenuFichierTelephones, true);
		$tableauContenuFichierMails = json_decode($contenuFichierMails, true);
		$tableauContenuFichierAdresses = json_decode($contenuFichierAdresses, true);

		/* on les met à jour */

		$tableauContenuFichierTelephones[$_POST["departement"]] = $_POST["numeroTelephone"];
		$tableauContenuFichierMails[$_POST["departement"]] = $_POST["mail"];
		$tableauContenuFichierAdresses[$_POST["departement"]] = $_POST["adresse"];

		/* et on les renvoie */

		$contenuFichierTelephones = json_encode($tableauContenuFichierTelephones);
		$contenuFichierMails = json_encode($tableauContenuFichierMails);
		$contenuFichierAdresses = json_encode($tableauContenuFichierAdresses);

		file_put_contents("../donnees/contacts/telephones.json" , $contenuFichierTelephones);
		file_put_contents("../donnees/contacts/mails.json" , $contenuFichierMails);
		file_put_contents("../donnees/contacts/adresses.json" , $contenuFichierAdresses);

		$_SESSION["notification"] = [ "message" => "Modifications effectuées", "type" => "ok" ];
	} else {
		$_SESSION["notification"] = [ "message" => "Erreur dans la modification", "type" => "pasOk" ];
	}

	header('Location: dashboard.php?onglet=contactModifier');
	exit();
?>
