<?php
	/* on vérifie que tout va bien */
	session_start();
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] == 'POST') || !isset($_POST['validerFormationCreer'])){
		session_destroy();
		header('Location: ../formation.php');
		exit();
	}

	if(!nomEstValide($_POST["nom"])){ // on vérifie que le nom est valide
		$_SESSION["notification"] = [ "message" => "Le nom donné n'est pas valide", "type" => "pasOk" ];
	} else if(!file_exists("../donnees/formations/".strtolower($_POST["nom"]).".json")){ // si le nom est libre et pas déjà pris

		/* on génère le tableau associé du formulaire */
		$tableauFormulaire = array_slice($_POST, 0, 5);
		$tableauFormulaire += [ "pdfAssocie" => "faux"];
		$tableauFormulaire += [ "dateModification" => $_POST["dateModification"]];
		if(count($_POST)>6){ // on fait attention au cas où y a des informations (catégories et parties) associées à la formation
			$tableauFormulaire += ["informations" => array_slice($_POST, 5, count($_POST)-5-1-1)];
		} else{
			$tableauFormulaire += ["informations" => []];
		}
		$tableauFormulaire += ["sessions" => []];

		$_SESSION["notification"] = [ "message" => "Formation créée !", "type" => "ok" ];

		if($_FILES["pdf"]["size"] != 0){ // s'il y a un pdf qui a été envoyé on le génère en le déplaçant du dossier temporaire d'upload (vidé à la fin de ce script) notre dossier de formations
			if(move_uploaded_file($_FILES["pdf"]["tmp_name"], "../donnees/formations/".strtolower($_POST["nom"]).".pdf")){
				$tableauFormulaire["pdfAssocie"] = "vrai";
			} else{
				$_SESSION["notification"] = [ "message" => "La formation a été créée, mais le .pdf n'a pas pu être téléchargé !", "type" => "pasOk" ];
			}
		}

		$jsonFormulaire = json_encode($tableauFormulaire);
		file_put_contents("../donnees/formations/".strtolower($_POST["nom"]).".json" , $jsonFormulaire);
		
	} else {
		$_SESSION["notification"] = [ "message" => "La formation existe déjà", "type" => "pasOk" ];
	}
	
	header('Location: dashboard.php?onglet=formationCreer');
	exit();

	function nomEstValide($nom){
		return (
			strpos($nom, '.') === false &&
			strpos($nom, '>') === false &&
			strpos($nom, '<') === false &&
			strpos($nom, ':') === false &&
			strpos($nom, '?') === false &&
			strpos($nom, '*') === false &&
			strpos($nom, '/') === false &&
			strpos($nom, '\\') === false &&
			strpos($nom, '|') === false &&
			strpos($nom, '"') === false &&
			strpos($nom, '&') === false
		);
	}
?>
