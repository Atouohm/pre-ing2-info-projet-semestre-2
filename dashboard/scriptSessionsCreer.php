<?php
	session_start(); // on ouvre une session (s'il y en avait déjà eu on l'ouvre dans cette page)
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] == 'POST') || !isset($_POST['validerSessionsCreer'])){ // si la session n'est pas valide ou que on a pas les données qu'il faut pour la suite du script
		session_destroy(); // on déconnecte
		header('Location: ../formation.php'); // et on redirige vers l'acceuil du portail des formations
		exit();
	}

	if(file_exists("../donnees/formations/".strtolower($_POST["nomFormationAssociee"]).".json")){ // si le fichier de al formation existe
		
		/* on récupère les donnnées */
		$contenuFichier = file_get_contents("../donnees/formations/".strtolower($_POST["nomFormationAssociee"]).".json");
		$tableauContenuFichier = json_decode($contenuFichier);

		/* on met à jour la partie session de l'objet contenu du fichier (c'est un objet car on a pas mis json_decode "true") */
		$tableauContenuFichier->sessions[] = (object) array('date' => $_POST["date"], 'horaire' => $_POST["horaire"], 'ville' => $_POST["ville"], 'adresse' => $_POST["adresse"], 'codePostal' => $_POST["codePostal"], 'lienInscription' => $_POST["lienInscription"]);

		/* on envoie dans le fichier de la formation */
		$contenuFichier = json_encode($tableauContenuFichier);
		file_put_contents("../donnees/formations/".strtolower($_POST["nomFormationAssociee"]).".json" , $contenuFichier);

		$_SESSION["notification"] = [ "message" => "Modifications effectuées", "type" => "ok" ];
	} else {
		$_SESSION["notification"] = [ "message" => "Erreur dans la modification", "type" => "pasOk" ];
	}

	header('Location: dashboard.php?onglet=sessionsModifier');
	exit();
?>