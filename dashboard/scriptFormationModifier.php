<?php
	
	/* on regarde si tout est ok */
	session_start();
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] == 'POST') || !isset($_POST['validerFormationModifier'])){
		session_destroy();
		header('Location: ../formation.php');
		exit();
	}

	/* on fait le traitement */
	if(!nomEstValide($_POST["nom"])){ // on vérifie que le nom est valide
		$_SESSION["notification"] = [ "message" => "Le nom donné n'est pas valide", "type" => "pasOk" ];
	} else if(file_exists("../donnees/formations/".strtolower($_POST["nomInitial"]).".json") && !($_POST["nom"]!=$_POST["nomInitial"] && file_exists("../donnees/formations/".strtolower($_POST["nom"]).".json"))){ // on vérifie que  le fichier initial existe et que si le nouveau nom est différent de l'initial, il ne soit pas déjà pris

		/* on récupère les données initiales et supprime le fichier*/
		$contenuFichier = file_get_contents("../donnees/formations/".strtolower($_POST["nomInitial"]).".json");
		unlink("../donnees/formations/".strtolower($_POST["nomInitial"]).".json");

		$tableauContenuFichier = json_decode($contenuFichier, true);

		/* on génère le tableau associé au formulaire */
		$tableauFormulaire = array_slice($_POST, 0, 5);
		$tableauFormulaire += [ "pdfAssocie" => $tableauContenuFichier["pdfAssocie"]];
		$tableauFormulaire += [ "dateModification" => $_POST["dateModification"]];
		if($_POST["menuModificationPdfActive"] == "vrai"){ // on fait attention aux cas si le pdf doit être modifier ou non et s'il y a des informations associées à la formation ou non
            if(count($_POST)>8){
                $tableauFormulaire += ["informations" => array_slice($_POST, 6, count($_POST)-5-1-1-1)];
            } else{
                $tableauFormulaire += ["informations" => []];
            }
        } else{
            if(count($_POST)>7){
                $tableauFormulaire += ["informations" => array_slice($_POST, 5, count($_POST)-5-1-1-1)];
            } else{
                $tableauFormulaire += ["informations" => []];
            }
        }
		$tableauFormulaire += ["informations" => $_POST["pdf"]];
		$tableauFormulaire += ["sessions" => $tableauContenuFichier["sessions"]];

		$_SESSION["notification"] = [ "message" => "Modifications effectuées !", "type" => "ok" ]; // on prépare la notification

		/* si le pdf doit être modifié on le fait*/
		if($_POST["menuModificationPdfActive"] == "vrai"){
			if(file_exists("../donnees/formations/".strtolower($_POST["nomInitial"]).".pdf")){ // on supprime l'ancien
				unlink("../donnees/formations/".strtolower($_POST["nomInitial"]).".pdf");
				$tableauFormulaire["pdfAssocie"] = "faux";
			}
			if($_FILES["pdf"]["size"] != 0){ // s'il y a un nouveau on le met
				if(move_uploaded_file($_FILES["pdf"]["tmp_name"], "../donnees/formations/".strtolower($_POST["nom"]).".pdf")){
					$tableauFormulaire["pdfAssocie"] = "vrai";
				} else{
					$_SESSION["notification"] = [ "message" => "La formation a été créée, mais le .pdf n'a pas pu être téléchargé !", "type" => "pasOk" ];
				}
			}
		} else if(file_exists("../donnees/formations/".strtolower($_POST["nomInitial"]).".pdf")){ // même s'il ne doit pas être modifier on le rename comme il faut
			$contenuPdf = file_get_contents("../donnees/formations/".strtolower($_POST["nomInitial"]).".pdf");
			unlink("../donnees/formations/".strtolower($_POST["nomInitial"]).".pdf");
			file_put_contents("../donnees/formations/".strtolower($_POST["nom"]).".pdf" , $contenuPdf);
		}

		/* on génère le fichier json de la formation modifiée */
		$jsonFormulaire = json_encode($tableauFormulaire);
		file_put_contents("../donnees/formations/".strtolower($_POST["nom"]).".json" , $jsonFormulaire);

	} else if(file_exists("../donnees/formations/".strtolower($_POST["nomInitial"]).".json") && ($_POST["nom"]!=$_POST["nomInitial"] && file_exists("../donnees/formations/".strtolower($_POST["nom"]).".json"))){
		$_SESSION["notification"] = [ "message" => "Une formation porte déjà ce nouveau nom", "type" => "pasOk" ];
	} else {
		$_SESSION["notification"] = [ "message" => "Erreur dans la modification", "type" => "pasOk" ];
	}

	header('Location: dashboard.php?onglet=formationModifier');
	exit();

	function nomEstValide($nom){
		return (
			strpos($nom, '.') === false &&
			strpos($nom, '>') === false &&
			strpos($nom, '<') === false &&
			strpos($nom, ':') === false &&
			strpos($nom, '?') === false &&
			strpos($nom, '*') === false &&
			strpos($nom, '/') === false &&
			strpos($nom, '\\') === false &&
			strpos($nom, '|') === false &&
			strpos($nom, '"') === false &&
			strpos($nom, '&') === false
		);
	}
?>
