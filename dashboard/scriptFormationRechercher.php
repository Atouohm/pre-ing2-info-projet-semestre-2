<?php
	$lienDossier = '../donnees/formations/';
	$dossier = opendir($lienDossier);

	$tableauDonnees = [];

	while($nomElement = readdir($dossier)){ // on parcourt tout les fichier du dossier des formations
		if($nomElement != '.' && $nomElement != '..' && strpos($nomElement, ".json") != false){ // si c'est un json

			/* on récupère les données */
			$contenuFichier = file_get_contents("../donnees/formations/".$nomElement);
			$donneesFichier = json_decode($contenuFichier);
			$tableauDonnees[] = [ "nom" => $donneesFichier->{"nom"}, "description" => $donneesFichier->description, "format" => $donneesFichier->format, "groupe" => $donneesFichier->groupe, "duree" => $donneesFichier->duree, "pdfAssocie" => $donneesFichier->pdfAssocie, "informations" => $donneesFichier->informations];
		}
	}

	echo(json_encode($tableauDonnees)); // envoie les données récupérées de la formation

	closedir($dossier);
?>
