<?php

	/* on regarde si l'utilisateur à le droite d'être là et si on a les infos nécessaires à la suite du script */
	session_start();
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] === 'POST') || !isset($_POST['validerInformationsEssentiellesAjouter'])){
		session_destroy();
		header('Location: ../formation.php');
		exit();
	}

	if(file_exists("../donnees/informationsEssentielles/informationsEssentielles.json")){
		unlink("../donnees/informationsEssentielles/informationsEssentielles.json");
	}

	$tableauContenuFichier = array_slice($_POST, 0, count($_POST)-1); // crée un tableau contenant les informations essentielles du formulaire envoyé

	/* et on le met dans le fichier des informations essentielles */
	$contenuFichier = json_encode($tableauContenuFichier);
	file_put_contents("../donnees/informationsEssentielles/informationsEssentielles.json" , $contenuFichier);

	$_SESSION["notification"] = [ "message" => "Modifications effectuées", "type" => "ok" ];

	header('Location: dashboard.php?onglet=informationsEssentiellesModifier');
	exit();
?>
