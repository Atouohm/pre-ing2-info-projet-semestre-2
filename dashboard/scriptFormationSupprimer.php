<?php

	/* on vérifie que tout va bien */
	session_start();
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] == 'POST') || !isset($_POST["nom"])){
		http_response_code(400);
		exit("erreur");
	}

	if(file_exists("../donnees/formations/".strtolower($_POST["nom"]).".json")){ // si le fichier de la formation existe

		unlink("../donnees/formations/".strtolower($_POST["nom"]).".json"); // on le supprime

		echo(json_encode([ "message" => "Modifications effectuées", "type" => "ok" ])); // on renvoie les données nécessaire à l'affichage d'une notification pour la page js que a demandé ce script
	} else {
		echo(json_encode([ "message" => "Erreur dans la modification", "type" => "pasOk" ]));
	}
?>
