<?php
	session_start();
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] === 'POST') || !isset($_POST["nomFormationAssociee"])){ // on vérifie que tout est bon
		http_response_code(400); // sinon on dit que c'est une bad request
		exit("erreur"); // et on quitte
	}

	if(file_exists("../donnees/formations/".strtolower($_POST["nomFormationAssociee"]).".json")){ // si le fichier de la formation existe

		/* on récupère le contenu */
		$contenuFichier = file_get_contents("../donnees/formations/".strtolower($_POST["nomFormationAssociee"]).".json");
		$tableauContenuFichier = json_decode($contenuFichier);

		$nouveauTableauSessionsTableauContenuFichier = [];

		/* on parcourt toutes les sessions et si on est face à la session à supprimer on la supprimer */
		for($i=0 ; $i<count($tableauContenuFichier->sessions) ; $i++){
			if($i != $_POST["numeroSessionASupprimer"]){
				$nouveauTableauSessionsTableauContenuFichier[] = $tableauContenuFichier->sessions[$i];
			}

		}

		$tableauContenuFichier->sessions = $nouveauTableauSessionsTableauContenuFichier; // on met à jour les sessions dans la tableau décode du fichier

		/* on l'encode et on le réinjecte */
		$contenuFichier = json_encode($tableauContenuFichier);
		file_put_contents("../donnees/formations/".strtolower($_POST["nomFormationAssociee"]).".json" , $contenuFichier);

		echo(json_encode([ "message" => "Modifications effectuées", "type" => "ok" ]));
	} else {
		echo(json_encode([ "message" => "Erreur dans la modification", "type" => "pasOk" ]));
	}
?>
