<?php
	/* on vérifie que tout va bien */
	session_start();
	if(!isset($_SESSION["identifiant"]) || !($_SERVER['REQUEST_METHOD'] == 'POST') || !isset($_POST['validerFormationDupliquer'])){
		session_destroy();
		header('Location: ../formation.php');
		exit();
	}

	if(!nomEstValide($_POST["nouveauNom"])){ // on vérifie que le nom de la formation est valide
		$_SESSION["notification"] = [ "message" => "Le nom donné n'est pas valide", "type" => "pasOk" ];
	} else if(file_exists("../donnees/formations/".strtolower($_POST["nomInitial"]).".json") && !file_exists("../donnees/formations/".strtolower($_POST["nouveauNom"]).".json")){ // on regarque le que nouveau nom ne soit pas pris (et que le fichier initial existe bien)

		/* on copie les données en changeant le nom et la date de modification */
		$contenuFichier = file_get_contents("../donnees/formations/".strtolower($_POST["nomInitial"]).".json");
		$tableauContenuFichier = json_decode($contenuFichier, true);
		$tableauContenuFichier["nom"] = $_POST["nouveauNom"];
		$tableauContenuFichier["dateModification"] = $_POST["dateModification"];

		/* s'il y avait un pdf associé on le duplique aussi */
		if($tableauContenuFichier["pdfAssocie"] == "vrai"){
			$contenuPdf = file_get_contents("../donnees/formations/".strtolower($_POST["nomInitial"]).".pdf");
			file_put_contents("../donnees/formations/".strtolower($_POST["nouveauNom"]).".pdf" , $contenuPdf);
		}

		$contenuFichier = json_encode($tableauContenuFichier);
		file_put_contents("../donnees/formations/".strtolower($_POST["nouveauNom"]).".json" , $contenuFichier);

		$_SESSION["notification"] = [ "message" => "Modifications effectuées", "type" => "ok" ];
	} else if(file_exists("../donnees/formations/".strtolower($_POST["nomInitial"]).".json") && file_exists("../donnees/formations/".strtolower($_POST["nouveauNom"]).".json")){
		$_SESSION["notification"] = [ "message" => "Une formation porte déjà ce nouveau nom", "type" => "pasOk" ];
	} else {
		$_SESSION["notification"] = [ "message" => "Erreur dans la modification", "type" => "pasOk" ];
	}

	header('Location: dashboard.php?onglet=formationModifier');
	exit();

	function nomEstValide($nom){
		return (
			strpos($nom, '.') === false &&
			strpos($nom, '>') === false &&
			strpos($nom, '<') === false &&
			strpos($nom, ':') === false &&
			strpos($nom, '?') === false &&
			strpos($nom, '*') === false &&
			strpos($nom, '/') === false &&
			strpos($nom, '\\') === false &&
			strpos($nom, '|') === false &&
			strpos($nom, '"') === false &&
			strpos($nom, '&') === false
		);
	}
?>
