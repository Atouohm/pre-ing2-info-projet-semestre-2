/* variables */
const formFormation = document.querySelector("#formationModifier .formFormation");
const boiteDesSessions = document.querySelector("#sessionsModifier .boiteDesSessions");
const boiteListeDesSessions = document.querySelector("#sessionsModifier .boiteListeDesSessions");
let conteneurInputsFormation = document.querySelector("#formationModifier .conteneurInputsFormation");
let tableauResultatsRechercheFormations;
let tableauListeDesSessions;


/* pour le dashboard au global */

document.querySelectorAll('.dateModification').forEach(input => {
	input.value = new Date().toLocaleDateString();
});

/* système pour ajoute un autoresize à tous les textarea du dashboard */
ajouterEventListenerAuxTextarea();

function ajouterEventListenerAuxTextarea(){
	document.querySelectorAll("textarea").forEach( textarea => {
		textarea.addEventListener('input', autoResizeAvecEvent);
	});
}

function autoResizeAvecEvent(event){
	autoResizeSansEvent(event.target);
}

function autoResizeSansEvent(textarea){ // pour le faire manuellement plua tard si besoin
	textarea.style.height = "auto";
	textarea.style.height = textarea.scrollHeight + "px";
}

/* fonction qui automatise la selection dans la barre de navigation */
function navLienSelection(navLien){
	let navLiens = document.getElementsByClassName("nav-liens");
	let onglets = document.getElementsByClassName("onglet");
	if(!navLien.classList.contains("actif")){
		navLien.classList.toggle("actif");
		navLien.removeAttribute("onclick");
		for(let i=0 ; i<navLiens.length ; i++){
			if(navLiens[i] != navLien && navLiens[i].classList.contains("actif")){
				navLiens[i].classList.toggle("actif");
				onglets[i].classList.toggle("actif");
				navLiens[i].setAttribute("onclick", "navLienSelection(this)")
			}
		}
		document.querySelector(`.onglet[data-indice-onglet='${navLien.dataset.indiceOnglet}']`).classList.toggle("actif");

		responsiveNavbarre();
	}
}

/* fonction toggle le hamburger (que ce soit le fermer ou l'ouvrir) */
function hamburger(bouton){
	let navbarre = document.getElementById("navbarre");
	let main = document.querySelector("main");
	if(bouton.classList.contains("ferme")){
		bouton.classList.toggle("ferme");
		bouton.classList.toggle("ouvert");

		navbarre.classList.toggle("fermee");
		navbarre.classList.toggle("ouverte");

		main.classList.toggle("navbarreFermee");
		main.classList.toggle("navbarreOuverte");
	} else{
		bouton.classList.toggle("ouvert");
		bouton.classList.toggle("ferme");

		navbarre.classList.toggle("ouverte");
		navbarre.classList.toggle("fermee");

		main.classList.toggle("navbarreOuverte");
		main.classList.toggle("navbarreFermee");
	}
}

/* permet de cacher la navbarre si la largeur du viewport est trop petite */
function responsiveNavbarre(){
	let bouton = document.getElementById("hamburger");
	let navbarre = document.getElementById("navbarre");
	let main = document.querySelector("main");
	if(window.innerWidth<1100 && main.classList.contains("navbarreOuverte")){
		bouton.classList.toggle("ouvert");
		bouton.classList.toggle("ferme");

		navbarre.classList.toggle("ouverte");
		navbarre.classList.toggle("fermee");

		main.classList.toggle("navbarreOuverte");
		main.classList.toggle("navbarreFermee");
	}
}

/* on ajuste la navbar au chargement de la page maus aussi quand le viewport change de taille */
window.onload = responsiveNavbarre;
window.onresize = responsiveNavbarre;

/* ajout d'event listener pour les boutons de fermetures des popups */
document.querySelectorAll('.popupBoite .boutonFermer').forEach(bouton => {
	bouton.addEventListener('click', () => {
		declencherPopup(bouton.parentNode.parentNode.getAttribute("id"));
	})
});

document.querySelectorAll('.popupBoite .boiteChoix:not(.dansUnForm) .boutonAnnuler').forEach(bouton => {
	bouton.addEventListener('click', () => {
		declencherPopup(bouton.parentNode.parentNode.parentNode.getAttribute("id"));
	})
});

document.querySelectorAll('.popupBoite .boiteChoix.dansUnForm .boutonAnnuler').forEach(bouton => {
	bouton.addEventListener('click', () => {
		declencherPopup(bouton.parentNode.parentNode.parentNode.parentNode.getAttribute("id"));
	})
});

/* la fonction qui toggle une popup (ouvre ou ferme suivant le cas initial) */
function declencherPopup(nomPopup){
	document.getElementById(nomPopup).classList.toggle("ouverte");
	document.querySelector(".fondFlou").classList.toggle("actif");
}

/* fonction qui automatise l'affichage d'une notification */
function afficherNotification(message, type){
	let notification = document.getElementById("notification");
	notification.innerHTML = message;

	notification.removeAttribute("class");
	window.requestAnimationFrame( () => {
		window.requestAnimationFrame( () => {
			notification.setAttribute("class", "afficher" + " " + type);
		});
	});
}

/* commun à l'onglet de création de formations et à celui de modifications de formations */

/* supprime une catégorie en renumérant les autres y comprisent les parties des chaque catégories numérotées */
function supprimerCategorie(categorie){
	let listeCategories = categorie.parentNode;
	categorie.remove();

	for(let i=0 ; i<listeCategories.childElementCount ; i++){
		let categorieAMettreAJour = listeCategories.children[i];
		categorieAMettreAJour.querySelector(".titreCategorie").name = `c${i+1}Titre`;
		categorieAMettreAJour.querySelector(".descriptionCategorie").name = `c${i+1}Description`;

		reNumeroterParties(categorieAMettreAJour.querySelector(".listeParties"), i+1);
	}
}

/* supprimer une partie et renumérote les autres de la même catégorie */
function supprimerPartie(partie){
	let listeParties = partie.parentNode;
	let numeroCategorie = parseInt(partie.querySelector(".titrePartie").name.slice(1, partie.querySelector(".titrePartie").name.length));
	partie.remove();
	reNumeroterParties(listeParties, numeroCategorie);
}

/* renumerote les parties d'une catégorie */
function reNumeroterParties(listeParties, numeroCategorie){
	for(let i=0 ; i<listeParties.childElementCount ; i++){
		let partieAMettreAJour = listeParties.children[i];
		partieAMettreAJour.querySelector(".titrePartie").name = `c${numeroCategorie}p${i+1}Titre`;
		partieAMettreAJour.querySelector(".descriptionPartie").name = `c${numeroCategorie}p${i+1}Description`;
	}
}

/* ajoute les inputs d'une catégorie */
function ajouterUneCategorie(listeCategories){
	let numeroNouvelleCategorie = listeCategories.childElementCount + 1; // on récupère le numéro que va avoir la nouvelle catégorie

	let categorie = document.createElement("div");
	categorie.setAttribute("class", "categorie");

	categorie.innerHTML = `
		<div class="input">
			<div class="enTete">
				<label>Catégorie (Titre) :</label>
				<span class="boutonAjouter boutonSpan" onclick="ajouterUneCategorie(this.parentNode.parentNode.parentNode.parentNode)">Ajouter une Catégorie</span>
				<span class="boutonAjouter boutonSpan" onclick="ajouterUnePartie(this.parentNode.parentNode.parentNode)">Ajouter une Partie à cette Catégorie</span>
				<span class="boutonSupprimer boutonSpan" onclick="supprimerCategorie(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
			</div>
			<input class="titreCategorie" name="c${numeroNouvelleCategorie}Titre" type="text" required>
		</div>
		<div class="input">
			<div class="enTete">
				<label>Catégorie (Description) :</label>
				<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
			</div>
			<textarea class="descriptionCategorie" name="c${numeroNouvelleCategorie}Description"></textarea>
		</div>
		<div class="listeParties">
			<div class="partie">
				<div class="input">
					<div class="enTete">
						<label>Catégorie (Partie -> Titre) :</label>
						<span class="boutonSupprimer boutonSpan" onclick="supprimerPartie(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
					</div>
					<input class="titrePartie" name="c${numeroNouvelleCategorie}p1Titre" type="text" required>
				</div>
				<div class="input">
					<div class="enTete">
						<label>Catégorie (Partie -> Description) :</label>
						<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
					</div>
					<textarea class="descriptionPartie" name="c${numeroNouvelleCategorie}p1Description"></textarea>
				</div>
			</div>
		</div>`;

	listeCategories.appendChild(categorie);
}

/* ajoute les inputs d'une partie à une catégorie */
function ajouterUnePartie(categorie){
	let listeParties = categorie.querySelector(".listeParties");
	let numeroCategorie = parseInt(categorie.querySelector(".titreCategorie").name.slice(1, categorie.querySelector(".titreCategorie").name.length)); // on récupère le numéro de la catégorie dans laquelle est la partie que l'on va ajouter
	let numeroNouvellePartie = listeParties.childElementCount + 1;

	let partie = document.createElement("div");
	partie.setAttribute("class", "partie");

	partie.innerHTML = `
		<div class="input">
			<div class="enTete">
				<label>Catégorie (Partie -> Titre) :</label>
				<span class="boutonSupprimer boutonSpan" onclick="supprimerPartie(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
			</div>
			<input class="titrePartie" name="c${numeroCategorie}p${numeroNouvellePartie}Titre" type="text" required>
		</div>
		<div class="input">
			<div class="enTete">
				<label>Catégorie (Partie -> Description) :</label>
				<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
			</div>
			<textarea class="descriptionPartie" name="c${numeroCategorie}p${numeroNouvellePartie}Description"></textarea>
		</div>`;

	listeParties.appendChild(partie);
}

/* commun à tous les onglets de modification */

/* fonction asynchrone qui recherche toutes les formations */
async function rechercherFormations(boiteResultatsRechercheFormations){
	const res = await fetch("scriptFormationRechercher.php");

	if(res.ok){
		const resJSON = await res.json();
		tableauResultatsRechercheFormations = resJSON;
		boiteResultatsRechercheFormations.innerHTML = "";
		creerListeFormations(tableauResultatsRechercheFormations, boiteResultatsRechercheFormations);
	} else{
		throw new Error("Erreur avec le script de recherche des formations.");
	}
}

/* fonction qui crée la liste (en html) de toutes les formations */
function creerListeFormations(tableauFormations, boiteResultatsRechercheFormations){
	tableauFormations.forEach( formation => {
		let itemFormation = document.createElement("div");
		itemFormation.setAttribute("class", "itemFormation");
		itemFormation.innerHTML = `
			<span>${formation.nom}</span>
		`

		idOnglet = boiteResultatsRechercheFormations.parentNode.parentNode.parentNode.getAttribute("id");
		if(idOnglet == "formationModifier"){
			itemFormation.setAttribute("onclick", "genererFormulaireModications(this)");
		} else if(idOnglet == "sessionsModifier"){
			itemFormation.setAttribute("onclick", "genererItemsSessions(this)");
		}

		boiteResultatsRechercheFormations.appendChild(itemFormation);
	});
}

/* on ajoute un event listener à l'input recherche de façon à filtrer dès que l'on intéragit avec l'input de recherche */
document.querySelectorAll(".inputRecherche input").forEach(inputRecherche => {
	inputRecherche.addEventListener("input", filtrerResultatsFormations);
});

/* fonction qui automatise le filtrage des formations */
function filtrerResultatsFormations(evenement){
	let idOnglet = evenement.target.parentNode.parentNode.parentNode.parentNode.getAttribute("id"); // on regarde dans quel onglet on est
	let boiteResultatsRechercheFormations = document.querySelector(`#${idOnglet} .boiteResultatsRechercheFormations`);

	/* suivant l'onglet où on est on fait un traitement un peu différent */
	if(idOnglet == "formationModifier"){
		desactiverBoutonsActionsRapides();
		conteneurInputsFormation.innerHTML = `<p style="margin-top: 20px; margin-bottom: 20px; text-align: center;">Sélectionnez une formation à votre gauche.</p>`;
	} else if(idOnglet == "sessionsModifier"){
		desactiverBoutonsActions();
		document.querySelector("#sessionsModifier .boiteDesSessions .boiteListeDesSessions").innerHTML = "";
	}

	boiteResultatsRechercheFormations.innerHTML = ""; // on vide la boite de la liste des formations

	const texteRecherche = evenement.target.value.toLowerCase().replace(/\s/g, ""); // on enlève les espaces du texte de recherche

	/* on filtre suivant si le texte de recherche s'accorde avec le nom de la formation */
	let tableauResultatsRechercheFormationsFiltre = tableauResultatsRechercheFormations.filter( formation => {
		return formation.nom.toLowerCase().replace(/\s/g, "").includes(texteRecherche);
	});

	creerListeFormations(tableauResultatsRechercheFormationsFiltre, boiteResultatsRechercheFormations); // on recrée la liste avec le tableau des formations filtré
}

/* pour la page formationModifier.html */

rechercherFormations(document.querySelector("#formationModifier .boiteResultatsRechercheFormations"));

/* fonctions qui permettent d'activer ou de désactiver les boutons d'actions rapides de la page formationModifier.html */
function activerBoutonsActionsRapides(){
	formFormation.querySelector("button").disabled = false;
	formFormation.querySelector("button").classList.remove("desactive");
	formFormation.querySelectorAll(".logosActionsRapides img")[0].removeAttribute("class");
	formFormation.querySelectorAll(".logosActionsRapides img")[1].removeAttribute("class");
}

function desactiverBoutonsActionsRapides(){
	formFormation.querySelector("button").disabled = true;
	formFormation.querySelector("button").classList.add("desactive");
	formFormation.querySelectorAll(".logosActionsRapides img")[0].setAttribute("class", "desactive");
	formFormation.querySelectorAll(".logosActionsRapides img")[1].setAttribute("class", "desactive");
}

/* fonction qui génère automatique le formulaire pour faire les modifications */
function genererFormulaireModications(elementBoiteResultatsRechercheFormationsFormationModifier){
	elementBoiteResultatsRechercheFormationsFormationModifier.classList.add("selectionne");
	let boiteResultatsRechercheFormationsFormationModifier = elementBoiteResultatsRechercheFormationsFormationModifier.parentNode;

	boiteResultatsRechercheFormationsFormationModifier.querySelectorAll("div").forEach( (element) => {
		if(element.classList.contains("selectionne") && element != elementBoiteResultatsRechercheFormationsFormationModifier){
			element.classList.remove("selectionne");
		}
	});

	nomFormation = elementBoiteResultatsRechercheFormationsFormationModifier.querySelector('span').innerHTML;
	for(let i=0 ; i<tableauResultatsRechercheFormations.length ; i++){
		if(tableauResultatsRechercheFormations[i].nom == nomFormation){
			document.querySelector("#formationModifier .nomInitial").value = nomFormation;
			genererLesInputsFormulaireModications(tableauResultatsRechercheFormations[i]);
		}
	}
	activerBoutonsActionsRapides();
}

/* sous fonction de la fonction précédente qui génère les inputs */
function genererLesInputsFormulaireModications(formation){
	conteneurInputsFormation.innerHTML = ""; // on vide ce qu'il y a avait avant

	/* on met les inputs "classiques" */

	let inputNom = document.createElement("div");
	inputNom.setAttribute("class", "input");
	inputNom.innerHTML = `
		<label>Nom :</label>
		<input class="nom" name="nom" type="text" required placeholder="Nom...">
		<p class="informationsInput">ATTENTION les caractères suivants sont prohibés : . > < : ? * / \\ | " &</p>`;
	inputNom.querySelector(".nom").value = formation.nom;
	conteneurInputsFormation.appendChild(inputNom);

	let inputDescription = document.createElement("div");
	inputDescription.setAttribute("class", "input");
	inputDescription.innerHTML = `
		<label>Texte de description :</label>
		<textarea name="description" required>${formation.description}</textarea>`;
	conteneurInputsFormation.appendChild(inputDescription);

	let inputFormat = document.createElement("div");
	inputFormat.setAttribute("class", "input");
	inputFormat.innerHTML = `
		<label>Format :</label>
		<input class="format" name="format" type="text" required placeholder="Format...">`;
	inputFormat.querySelector(".format").value = formation.format;
	conteneurInputsFormation.appendChild(inputFormat);

	let inputGroupe = document.createElement("div");
	inputGroupe.setAttribute("class", "input");
	inputGroupe.innerHTML = `
		<label>Groupe :</label>
		<input class="groupe" name="groupe" type="text" required placeholder="Groupe...">`;
	inputGroupe.querySelector(".groupe").value = formation.groupe;
	conteneurInputsFormation.appendChild(inputGroupe);

	let inputDuree = document.createElement("div");
	inputDuree.setAttribute("class", "input");
	inputDuree.innerHTML = `
		<label>Durée :</label>
		<input class="duree" name="duree" type="text" required placeholder="Durée...">`;
	inputDuree.querySelector(".duree").value = formation.duree;
	conteneurInputsFormation.appendChild(inputDuree);

	let inputPdf = document.createElement("div");
	let referencePdf = "il n'y en a aucun";
	if(formation.pdfAssocie == "vrai"){
		let lienPdf = "../donnees/formations/" + formation.nom.toLowerCase() + ".pdf";
		referencePdf = `<a href="${lienPdf}" target="_blank">l'actuel</a>`;
	}
	inputPdf.setAttribute("class", "input");
	inputPdf.innerHTML = `
		<div class="enTete">
			<label>Fichier .pdf associé (${referencePdf}) : </label>
			<span class="boutonModifier boutonSpan" onclick="modifierFichierPdf(this.parentNode.parentNode)">Menu de modification</span>
		</div>`;
	conteneurInputsFormation.appendChild(inputPdf);

	let listeCategories = document.createElement("div");
	listeCategories.setAttribute("class", "listeCategories");
	conteneurInputsFormation.appendChild(listeCategories);
	listeCategories = conteneurInputsFormation.querySelector(".listeCategories");

	/* on génère les inputs des catégories et des parties suivant les données déjà présentent de la formation */

	let informationsFormation = formation.informations;
	if(informationsFormation.length == 0){ // s'il n'y a aucune catégorie ni partie on génère quand un input de base pour le cas où l'utilisateur veut ajouter des catégories et des parties

		let categorie = document.createElement("div");
		categorie.setAttribute("class", "categorie");
		categorie.innerHTML = `
			<div class="input">
				<div class="enTete">
					<label>Catégorie (Titre) :</label>
					<span class="boutonAjouter boutonSpan" onclick="ajouterUneCategorie(this.parentNode.parentNode.parentNode.parentNode)">Ajouter une Catégorie</span>
					<span class="boutonAjouter boutonSpan" onclick="ajouterUnePartie(this.parentNode.parentNode.parentNode)">Ajouter une Partie à cette Catégorie</span>
					<span class="boutonSupprimer boutonSpan" onclick="supprimerCategorie(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
				</div>
				<input class="titres titreCategorie" name="c1Titre" type="text" required>
			</div>
			<div class="input">
				<div class="enTete">
					<label>Catégorie (Description) :</label>
					<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
				</div>
				<textarea class="descriptionCategorie" name="c1Description"></textarea>
			</div>
			<div class="listeParties">
				<div class="partie">
					<div class="input">
						<div class="enTete">
							<label>Catégorie (Partie -> Titre) :</label>
							<span class="boutonSupprimer boutonSpan" onclick="supprimerPartie(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
						</div>
						<input class="titres titrePartie" name="c1p1Titre" type="text" required>
					</div>
					<div class="input">
						<div class="enTete">
							<label>Catégorie (Partie -> Description) :</label>
							<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
						</div>
						<textarea class="descriptionPartie" name="c1p1Description"></textarea>
					</div>
				</div>
			</div>`;
		listeCategories.appendChild(categorie);

	} else{ // sinon s'il y a déjà des infos
		for(let nomElement in informationsFormation){ // on les parcourt toutes
			if(nomElement.match(/^c[0-9]+Titre/) != null){ // si c'est le titre d'une catégorie on génère l'inpus associé et allant également chercher "en avance" la description

				let indiceCategorie = parseInt(nomElement.slice(1, nomElement.length));
				let categorie = document.createElement("div");
				categorie.setAttribute("class", "categorie");
				categorie.innerHTML = `
					<div class="input">
						<div class="enTete">
							<label>Catégorie (Titre) :</label>
							<span class="boutonAjouter boutonSpan" onclick="ajouterUneCategorie(this.parentNode.parentNode.parentNode.parentNode)">Ajouter une Catégorie</span>
							<span class="boutonAjouter boutonSpan" onclick="ajouterUnePartie(this.parentNode.parentNode.parentNode)">Ajouter une Partie à cette Catégorie</span>
							<span class="boutonSupprimer boutonSpan" onclick="supprimerCategorie(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
						</div>
						<input class="titreCategorie c${indiceCategorie}Titre" name="c${indiceCategorie}Titre" type="text" required>
					</div>
					<div class="input">
						<div class="enTete">
							<label>Catégorie (Description) :</label>
							<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
						</div>
						<textarea class="descriptionCategorie c${indiceCategorie}Description" name="c${indiceCategorie}Description"></textarea>
					</div>
					<div class="listeParties"></div>`;
				categorie.querySelector(".titreCategorie").value = informationsFormation[nomElement];
				categorie.querySelector(`.descriptionCategorie`).value = informationsFormation[`c${indiceCategorie}Description`];
				listeCategories.appendChild(categorie);

				autoResizeSansEvent(listeCategories.querySelector(`.c${indiceCategorie}Description`));

			} else if(nomElement.match(/^c[0-9]+p[0-9]+Titre/) != null){ // sinon si c'est le titre d'une partie on fait pareil mais en version partie

				let indiceCategorie = parseInt(nomElement.slice(1, nomElement.length));
				let indicePartie = parseInt(nomElement.match(/p[0-9]+/)[0].slice(1, nomElement.match(/p[0-9]+/)[0].length));
				let partie = document.createElement("div");
				partie.setAttribute("class", "partie");
				partie.innerHTML = `
					<div class="input">
						<div class="enTete">
							<label>Catégorie (Partie -> Titre) :</label>
							<span class="boutonSupprimer boutonSpan" onclick="supprimerPartie(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
						</div>
						<input class="titrePartie c${indiceCategorie}p${indicePartie}Titre" name="c${indiceCategorie}p${indicePartie}Titre" type="text" required>
					</div>
					<div class="input">
						<div class="enTete">
							<label>Catégorie (Partie -> Description) :</label>
							<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
						</div>
						<textarea class="descriptionPartie c${indiceCategorie}p${indicePartie}Description" name="c${indiceCategorie}p${indicePartie}Description"></textarea>
					</div>`;
				partie.querySelector(".titrePartie").value = informationsFormation[nomElement];
				partie.querySelector(`.descriptionPartie`).value = informationsFormation[`c${indiceCategorie}p${indicePartie}Description`];
				listeCategories.querySelector(`.c${indiceCategorie}Titre`).parentNode.parentNode.appendChild(partie);

				autoResizeSansEvent(listeCategories.querySelector(`.c${indiceCategorie}p${indicePartie}Description`));

			}
		}
	}

	ajouterEventListenerAuxTextarea();
}

/* fonction qui toggle le menu de modification du pdf */
function modifierFichierPdf(boiteInputPdf){
	let menuModificationPdf = boiteInputPdf.querySelector(".menuModificationPdf");
	if(menuModificationPdf != null){
		boiteInputPdf.removeChild(menuModificationPdf);
	} else{
		let menuModificationPdf = document.createElement("div");
		menuModificationPdf.setAttribute("class", "menuModificationPdf");
		menuModificationPdf.innerHTML = `
			<input name="pdf" type="file" accept=".pdf">
			<input type="hidden" name="menuModificationPdfActive" value="vrai">
			<p class="informationsInput">Pour supprimer le pdf actuel : ne mettre aucun fichier et laisser ce texte apparant.</p>`;
		boiteInputPdf.appendChild(menuModificationPdf);
	}
}

/* fonction qui active la popup de suppression de la formation actuellement sélectionnée */
function supprimerFormation(){
	let nomFormationASupprimer;
	let boiteResultatsRechercheFormationsFormationModifier = document.querySelector("#formationModifier .boiteResultatsRechercheFormations");

	boiteResultatsRechercheFormationsFormationModifier.querySelectorAll("div").forEach( (formation) => {
		if(formation.classList.contains("selectionne")){
			nomFormationASupprimer = formation.querySelector("span").innerHTML;
		}
	});

	document.getElementById("popupSupprimerFormation").querySelector(".nomFormation").innerHTML = nomFormationASupprimer;
	document.getElementById("popupSupprimerFormation").querySelector(".boutonValider").setAttribute("onclick", `supprimerFormationValidee("${nomFormationASupprimer}")`);
	declencherPopup("popupSupprimerFormation");
}

/* fonction asynchrone qui va envoyer la demande suppression de la formation au serveur */
async function supprimerFormationValidee(nomFormationASupprimer){
	let corpRequete = new FormData();
	corpRequete.append("nom", nomFormationASupprimer);

	const res = await fetch("scriptFormationSupprimer.php", {
		method: "POST",
		body: corpRequete
	});

	if(res.ok){
		const resJSON = await res.json();

		rechercherFormations(document.querySelector("#formationModifier .boiteResultatsRechercheFormations"));
		desactiverBoutonsActionsRapides();
		conteneurInputsFormation.innerHTML = `<p style="margin-top: 20px; margin-bottom: 20px; text-align: center;">Sélectionnez une formation à votre gauche.</p>`;

		rechercherFormations(document.querySelector("#sessionsModifier .boiteResultatsRechercheFormations"));
		desactiverBoutonsActions();
		boiteListeDesSessions.innerHTML = "";

		declencherPopup("popupSupprimerFormation");
		afficherNotification(resJSON["message"], resJSON.type);
	} else{
		declencherPopup("popupSupprimerFormation");
		throw new Error("Erreur avec le script de suppression.");
	}
}

/* fonction qui active la popup de duplication de la formation actuellement sélectionnée */
function dupliquerFormation(){
	let nomFormationADupliquer;
	let boiteResultatsRechercheFormationsFormationModifier = document.querySelector("#formationModifier .boiteResultatsRechercheFormations");

	boiteResultatsRechercheFormationsFormationModifier.querySelectorAll("div").forEach( (formation) => {
		if(formation.classList.contains("selectionne")){
			nomFormationADupliquer = formation.querySelector("span").innerHTML;
		}
	});

	document.getElementById("popupDupliquerFormation").querySelector(".nomFormation").innerHTML = nomFormationADupliquer;
	document.getElementById("popupDupliquerFormation").querySelector("form input.nomInitial").value = nomFormationADupliquer;
	declencherPopup("popupDupliquerFormation");
}

/* pour la page sessionsModifier.html */

rechercherFormations(document.querySelector("#sessionsModifier .boiteResultatsRechercheFormations"));

/* fonctions qui permettent d'activer ou de désactiver les boutons d'actions de la page sessionsModifier.html */
function activerBoutonCreerSession(){
	boiteDesSessions.querySelector(".boiteActions button.boutonCreer").disabled = false;
	boiteDesSessions.querySelector(".boiteActions button.boutonCreer").classList.remove("desactive");
}

function activerBoutonSupprimerSession(){
	boiteDesSessions.querySelector(".boiteActions button.boutonSupprimer").disabled = false;
	boiteDesSessions.querySelector(".boiteActions button.boutonSupprimer").classList.remove("desactive");
}

function desactiverBoutonsActions(){
	boiteDesSessions.querySelectorAll(".boiteActions button").forEach( (bouton) => {
		bouton.disabled = true;
		bouton.classList.add("desactive");
	});
}

/* fonction asynchrone qui demande au serveur les sessions associées à une formation données */
async function rechercherSessions(nomFormation){
	let corpRequete = new FormData();
	corpRequete.append("nom", nomFormation);

	const res = await fetch("scriptSessionsRechercher.php", {
		method: "POST",
		body: corpRequete
	});

	if(res.ok){
		const resJSON = await res.json();

		tableauListeDesSessions = resJSON;
		boiteListeDesSessions.innerHTML = "";
		creerListeSessions(tableauListeDesSessions);
	} else{
		throw new Error("Erreur avec le script de recherche des sessions.");
	}
}

/* fonction qui automatise la création des élements html des sessions */
function creerListeSessions(tableauSessions){
	tableauSessions.forEach( session => {
		let itemSession = document.createElement("div");
		itemSession.setAttribute("class", "itemSession");
		itemSession.setAttribute("onclick", "selectionSession(this)");
		itemSession.innerHTML = `
			<span class="date">${session.date}</span>

			<span class="ligneVerticale mini"></span>

			<span class="horaire">${session.horaire}</span>

			<span class="ligneVerticale mini"></span>

			<span class="ville">${session.ville}</span>

			<span class="ligneVerticale mini"></span>

			<span class="adresse">${session.adresse}</span>

			<span class="ligneVerticale mini"></span>

			<span class="codePostal">${session.codePostal}</span>

			<span class="ligneVerticale mini"></span>

			<a class="lienInscription" href="${session.lienInscription}" target="_blank">S'inscrire</a>
		`

		boiteListeDesSessions.appendChild(itemSession);
	});
}

/* fonction globale qui permet de générer les items html des sessions en lui passant simplement la boite des formations*/
function genererItemsSessions(elementBoiteResultatsRechercheFormationsSessionsModifier){
	elementBoiteResultatsRechercheFormationsSessionsModifier.classList.add("selectionne");
	let boiteResultatsRechercheFormationsSessionsModifier = elementBoiteResultatsRechercheFormationsSessionsModifier.parentNode;

	boiteResultatsRechercheFormationsSessionsModifier.querySelectorAll("div").forEach( (formation) => {
		if(formation.classList.contains("selectionne") && formation != elementBoiteResultatsRechercheFormationsSessionsModifier){
			formation.classList.remove("selectionne");
		}
	});

	nomFormation = elementBoiteResultatsRechercheFormationsSessionsModifier.querySelector('span').innerHTML;
	rechercherSessions(nomFormation);
	activerBoutonCreerSession();
}

/* automatise la sélection d'une session en activant le bouton de suppression */
function selectionSession(sessionSelectionnee){
	sessionSelectionnee.classList.add("selectionne");

	boiteListeDesSessions.querySelectorAll("div").forEach( (session) => {
		if(session.classList.contains("selectionne") && session != sessionSelectionnee){
			session.classList.remove("selectionne");
		}
	});

	activerBoutonSupprimerSession();
}

/* fonction qui ouvre la popup de suppression d'une session en récupérant le numéro de la session mais aussi le nom de la formation associée */
function supprimerSession(){
	let numeroSessionASupprimer;
	let nomFormationAssociee;
	let tableauSessions = boiteListeDesSessions.querySelectorAll("div");
	let boiteResultatsRechercheFormationsSessionsModifier = document.querySelector("#sessionsModifier .boiteResultatsRechercheFormations");

	for(let i=0 ; i<tableauSessions.length ; i++){
		if(tableauSessions[i].classList.contains("selectionne")){
			numeroSessionASupprimer = i;
		}
	}

	boiteResultatsRechercheFormationsSessionsModifier.querySelectorAll("div").forEach( (formation) => {
		if(formation.classList.contains("selectionne")){
			nomFormationAssociee = formation.querySelector("span").innerHTML;
		}
	});

	document.getElementById("popupSupprimerSession").querySelector(".boutonValider").setAttribute("onclick", `supprimerSessionValidee("${nomFormationAssociee}", "${numeroSessionASupprimer}")`);
	declencherPopup("popupSupprimerSession");
}

/* fonction qui envoie la demande de suppression d'une session au serveur */
async function supprimerSessionValidee(nomFormationAssociee, numeroSessionASupprimer){
	let corpRequete = new FormData();
	corpRequete.append("nomFormationAssociee", nomFormationAssociee);
	corpRequete.append("numeroSessionASupprimer", numeroSessionASupprimer);

	const res = await fetch("scriptSessionsSupprimer.php", {
		method: "POST",
		body: corpRequete
	});

	if(res.ok){
		const resJSON = await res.json();

		rechercherSessions(nomFormationAssociee);
		desactiverBoutonsActions();

		declencherPopup("popupSupprimerSession");
		afficherNotification(resJSON["message"], resJSON.type);
	} else{
		declencherPopup("popupSupprimerSession");
		throw new Error("Erreur avec le script de suppression.");
	}
}

/* fonction qui automatise la procédure d'affichage de la popup de création de session */
function creerSession(){
	let nomFormationAssociee;
	let boiteResultatsRechercheFormationsSessionsModifier = document.querySelector("#sessionsModifier .boiteResultatsRechercheFormations");

	boiteResultatsRechercheFormationsSessionsModifier.querySelectorAll("div").forEach( (formation) => {
		if(formation.classList.contains("selectionne")){
			nomFormationAssociee = formation.querySelector("span").innerHTML;
		}
	});

	document.getElementById("popupCreerSession").querySelector(".nomFormation").innerHTML = nomFormationAssociee;
	document.getElementById("popupCreerSession").querySelector(".nomFormationAssociee").value = nomFormationAssociee;
	declencherPopup("popupCreerSession");
}

/* pour la page contactModifier.html */

recupererContacts(document.querySelector("#contactModifier select")); // on récupère les contacts du département sélectionné

/* fonction qui récupère les contacts du département sélectionné */
async function recupererContacts(selecteur){
	let corpRequete = new FormData();
	corpRequete.append("numeroDepartement", selecteur.options[selecteur.selectedIndex].value);

	const res = await fetch("scriptContactRechercher.php", {
		method: "POST",
		body: corpRequete
	});

	if(res.ok){
		const donnees = await res.json();

		if(donnees.numero != ""){
			document.querySelector("#contactModifier input.numeroTelephone").value = donnees.numero;
		} else{
			document.querySelector("#contactModifier input.numeroTelephone").value = "";
		}

		if(donnees.mail != ""){
			document.querySelector("#contactModifier input.mail").value = donnees.mail;
		} else{
			document.querySelector("#contactModifier input.mail").value = "";
		}

		if(donnees.adresse != ""){
			document.querySelector("#contactModifier input.adresse").value = donnees.adresse;
		} else{
			document.querySelector("#contactModifier input.adresse").value = "";
		}

	} else{
		throw new Error("Erreur avec le script de recherche des contacts.");
	}
}

/* pour la page informationsEssentiellesModifier.html */

/* fonction qui ajoute les inputs nécessaires à l'ajout d'une information */
function ajouterInformation(listeInformations){
	let numeroNouvelleInformation = listeInformations.childElementCount + 1;

	let information = document.createElement("div");
	information.setAttribute("class", "information");

	information.innerHTML = `
		<div class="input">
			<div class="enTete">
				<label>Information (Titre) :</label>
				<span class="boutonSupprimer boutonSpan" onclick="supprimerInformation(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
			</div>
			<input class="titreInformation" name="i${numeroNouvelleInformation}Titre" type="text" required>
		</div>
		<div class="input">
			<div class="enTete">
				<label>Information (Description) :</label>
				<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
			</div>
			<textarea class="descriptionInformation" name="i${numeroNouvelleInformation}Description" required></textarea>
		</div>`;

	listeInformations.appendChild(information);
}

/* fonction qui supprimer une information et qui renumérote les autres */
function supprimerInformation(information){
	let listeInformations = information.parentNode;
	information.remove();

	for(let i=1 ; i<listeInformations.childElementCount ; i++){ // on prend pas la première car elle ne bouge jamais (pas supprimable)
		let informationAMettreAJour = listeInformations.children[i];
		informationAMettreAJour.querySelector(".titreInformation").name = `i${i+1}Titre`;
		informationAMettreAJour.querySelector(".descriptionInformation").name = `i${i+1}Description`;
	}
}

/* fonction asynchrone qui récupère les informations essentielles (elle doit être lancée quand l'onglet informations essentielles est affiché car sinon les autoresize des textarea ne marchera pas puisque qu'il n'ont pas de taille si pas affichés !) */
async function recupererInformationsEssentielles(){
	const res = await fetch("scriptInformationsEssentiellesRechercher.php");

	if(res.ok){
		const donnees = await res.json();

		genererLesInputsInformationsEssentielles(donnees);
		
	} else{
		throw new Error("Erreur avec le script de recherche des contacts.");
	}
}

/* génère les inputs des information essentielles que le serveur nous envoyé */
function genererLesInputsInformationsEssentielles(donnees){
	let boiteInputs = document.querySelector("#informationsEssentiellesModifier form .listeInformations");

	for(let nomElement in donnees){
		if(nomElement == "i1Titre"){

			boiteInputs.querySelector(".i1Titre").value = donnees[nomElement];

		} else if(nomElement == "i1Description"){

			boiteInputs.querySelector(".i1Description").value = donnees[nomElement];
			autoResizeSansEvent(boiteInputs.querySelector(".i1Description"));

		} else if(nomElement.match(/^i[0-9]+Titre/) != null){

			let indiceInformation = parseInt(nomElement.slice(1, nomElement.length));
			let information = document.createElement("div");
			information.setAttribute("class", "information");
			information.innerHTML = `
				<div class="input">
					<div class="enTete">
						<label>Information (Titre) :</label>
						<span class="boutonSupprimer boutonSpan" onclick="supprimerInformation(this.parentNode.parentNode.parentNode)"><span></span><span></span></span>
					</div>
					<input class="titreInformation i${indiceInformation}Titre" name="i${indiceInformation}Titre" type="text" required>
				</div>
				<div class="input">
					<div class="enTete">
						<label>Information (Description) :</label>
						<span class="boutonPoint boutonSpan" onclick="this.parentNode.nextElementSibling.value += '\\r\\n        &bull;  '; autoResizeSansEvent(this.parentNode.nextElementSibling); this.parentNode.nextElementSibling.focus()"><span></span></span>
					</div>
					<textarea class="descriptionInformation i${indiceInformation}Description" name="i${indiceInformation}Description" required></textarea>
				</div>`;
			information.querySelector(`.i${indiceInformation}Titre`).value = donnees[nomElement];
			information.querySelector(`.i${indiceInformation}Description`).value = donnees[`i${indiceInformation}Description`];

			boiteInputs.appendChild(information);
			autoResizeSansEvent(boiteInputs.querySelector(`.i${indiceInformation}Description`));

		}
	}
}