<?php
	session_start();
	if(!isset($_SESSION["identifiant"])){
		session_destroy();
		header('Location: ../formation.php');
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="./dashboard.css">
	<title>Gestionnaire des Formations</title>
</head>
<body>

	<!-- boite de la notification -->
	<div id="notification" <?php if(isset($_SESSION['notification'])){echo("class='afficher"." ".$_SESSION['notification']["type"]."'");} ?>><?php
		if(isset($_SESSION['notification'])){
			echo($_SESSION['notification']["message"]);
		}
		unset($_SESSION['notification']);
	?></div>

	<!-- boite ou plutôt cadre devant tout sauf les popups, il est transparent mais flou -->
	<div class="fondFlou"></div>

	<!-- plusieurs popups différentes mais avec toujours la même structure -->

	<div id="popupSupprimerFormation" class="popupBoite">
		<div class="popupContenu">
			<button type="button" class="boutonFermer"><span></span><span></span></button>
			<h3>La formation <span class="nomFormation" style="color: #0aA79B"></span> est sur le point d'être <span style="color: red">supprimée</span> !</h3>
			<div class="boiteChoix">
				<button class="boutonAnnuler boutonChoix">Annuler</button>
				<button class="boutonValider boutonChoix">Valider</button>
			</div>
		</div>
	</div>

	<div id="popupDupliquerFormation" class="popupBoite">
		<div class="popupContenu">
			<button type="button" class="boutonFermer"><span></span><span></span></button>
			<h3>La formation <span class="nomFormation" style="color: #0aA79B"></span> est sur le point d'être <span style="color: red">dupliquée</span> !</h3>
			<form action="scriptFormationDupliquer.php" method="POST">
				<div class="boiteInputNouveauNom">
					<input name="nouveauNom" type="text" placeholder="Nouveau nom..." required>
					<input type="hidden" class="dateModification" name="dateModification">
					<input type="hidden" class="nomInitial" name="nomInitial">
				</div>
				<div class="boiteChoix dansUnForm">
					<button type="button" class="boutonAnnuler boutonChoix">Annuler</button>
					<button type="submit" name="validerFormationDupliquer" class="boutonValider boutonChoix">Valider</button>
				</div>
			</form>
		</div>
	</div>

	<div id="popupCreerSession" class="popupBoite">
		<div class="popupContenu">
			<button type="button" class="boutonFermer"><span></span><span></span></button>
			<h3>Création d'une session pour la formation <span class="nomFormation" style="color: #0aA79B"></span> :</h3>
			<form action="scriptSessionsCreer.php" method="POST">
				<div class="boiteInputs">
					<label>Date</label>
					<input name="date" type="date" placeholder="ex: 00/00/0000" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" required>
					<label>Heure</label>
					<input name="horaire" type="time" placeholder="ex: 00:00" pattern="[0-9]{2}:[0-9]{2}" required>
					<label>Ville</label>
					<input name="ville" type="text" placeholder="Ville..." class="ville" required>
					<label>Adresse</label>
					<input name="adresse" type="text" placeholder="Adresse..." required>
					<label>Code Postal</label>
					<input name="codePostal" type="text" placeholder="Code Postal (à 5 chiffres)..." required>
					<label>Lien d'inscription</label>
					<input name="lienInscription" type="text" placeholder="Lien pour l'inscription..." class="lienInscription" required>
					<input class="nomFormationAssociee" type="hidden" name="nomFormationAssociee">
				</div>
				<div class="boiteChoix dansUnForm">
					<button type="button" class="boutonAnnuler boutonChoix">Annuler</button>
					<button type="submit" name="validerSessionsCreer" class="boutonValider boutonChoix">Valider</button>
				</div>
			</form>
		</div>
	</div>

	<div id="popupSupprimerSession" class="popupBoite">
		<div class="popupContenu">
			<button type="button" class="boutonFermer"><span></span><span></span></button>
			<h3>La session sélectionnée est sur le point d'être <span style="color: red">supprimée</span> !</h3>
			<div class="boiteChoix">
				<button class="boutonAnnuler boutonChoix">Annuler</button>
				<button class="boutonValider boutonChoix">Valider</button>
			</div>
		</div>
	</div>

	<!-- la barre de navigation latérale -->

	<nav id="navbarre" class="ouverte">

		<!-- l'hamburger -->
		<button id="hamburger" class="ouvert" type="button" onclick="hamburger(this)">
			<span></span>
			<span></span>
			<span></span>
		</button>

		<!-- le logo -->
		<a id="nav-icone" href="https://www.justicerestaurative.org/antenne-ifjr-sud-ouest/">
			<img src="../assets/nav-icone.png">
			<h1>Gestionnaire des Formations</h1>
		</a>

		<!-- les liens -->
		<a id="bouton-quitter" href="../formation.php">quitter</a>
		<a data-indice-onglet="1" class="nav-liens<?php if($_GET["onglet"] == "formationCreer" || !isset($_GET["onglet"])){echo " actif";}?>" onclick="navLienSelection(this)">
			<img src="../assets/creer.png" style="width: 28px; margin: 0 1px 0">
			<span>Créer une Formation</span>
		</a>
		<a data-indice-onglet="2" class="nav-liens<?php if($_GET["onglet"] == "formationModifier"){echo " actif";}?>" onclick="navLienSelection(this)">
			<img src="../assets/modifierVert.png">
			<span>Modifier une Formation</span>
		</a>
		<a data-indice-onglet="3" class="nav-liens<?php if($_GET["onglet"] == "sessionsModifier"){echo " actif";}?>" onclick="navLienSelection(this)">
			<img src="../assets/modifierRose.png">
			<span>Modifier les Sessions</span>
		</a>
		<a data-indice-onglet="4" class="nav-liens<?php if($_GET["onglet"] == "contactModifier"){echo " actif";}?>" onclick="navLienSelection(this)">
			<img src="../assets/modifierVert.png">
			<span>Modifier un Contact</span>
		</a>
		<a data-indice-onglet="5" class="nav-liens<?php if($_GET["onglet"] == "informationsEssentiellesModifier"){echo " actif";}?>" onclick="navLienSelection(this); recupererInformationsEssentielles();">
			<img src="../assets/modifierVert.png">
			<span>Modifier les Informations Essentielles</span>
		</a>

	</nav>

	<!-- le main contenant tous les onglets (1 seul est affiché à la fois) -->
	<main class="navbarreOuverte">
		<div data-indice-onglet="1" class="onglet <?php if($_GET["onglet"] == "formationCreer" || !isset($_GET["onglet"])){echo "actif";}?>" id="formationCreer">
			<?php
				include("../proteger/dashboard/formationCreer.html");
			?>
		</div>
		<div data-indice-onglet="2" class="onglet ongletDeModification <?php if($_GET["onglet"] == "formationModifier"){echo "actif";}?>" id="formationModifier">
			<?php
				include("../proteger/dashboard/formationModifier.html");
			?>
		</div>
		<div data-indice-onglet="3" class="onglet ongletDeModification <?php if($_GET["onglet"] == "sessionsModifier"){echo "actif";}?>" id="sessionsModifier">
			<?php
				include("../proteger/dashboard/sessionsModifier.html");
			?>
		</div>
		<div data-indice-onglet="4" class="onglet ongletDeModification <?php if($_GET["onglet"] == "contactModifier"){echo "actif";}?>" id="contactModifier">
			<?php
				include("../proteger/dashboard/contactModifier.html");
			?>
		</div>
		<div data-indice-onglet="5" class="onglet ongletDeModification <?php if($_GET["onglet"] == "informationsEssentiellesModifier"){echo "actif";}?>" id="informationsEssentiellesModifier">
			<?php
				include("../proteger/dashboard/informationsEssentiellesModifier.html");
			?>
		</div>
	</main>

	<script type="text/javascript" src="dashboard.js"></script>
</body>
</html>