<?php
	if(!($_SERVER['REQUEST_METHOD'] == 'POST') || !isset($_POST["nom"])){
		http_response_code(400); // s'il y a quelque chose qui va pas on return une bad request
		exit("erreur");
	}

	$lienDossier = '../donnees/formations/';
	$dossier = opendir($lienDossier);

	$tableauDonnees = [];

	while($nomElement = readdir($dossier)){ // on parcourt tout les dossier
		if($nomElement != '.' && $nomElement != '..' && $nomElement == strtolower($_POST["nom"]).".json"){ // si on est en face du bon fichier

			/* on récupère la tableau des sessions */
			$contenuFichier = file_get_contents("../donnees/formations/".$nomElement);
			$donneesFichier = json_decode($contenuFichier);
			$tableauDonnees = $donneesFichier->sessions;
		}
	}

	echo(json_encode($tableauDonnees)); // et on l'envoie

	closedir($dossier);
?>
