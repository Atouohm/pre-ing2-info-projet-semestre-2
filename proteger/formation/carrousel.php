<div class="conteneur-carrousel">
	<button type="button" id="flecheGauche" class="fleche">
		<span></span>
		<span></span>
	</button>

	<button type="button" id="flecheDroite" class="fleche">
		<span></span>
		<span></span>
	</button>

	<ul class="carrousel">

		<?php
			$lienDossier = './donnees/formations/';
			$dossier = opendir($lienDossier); // on ouvre le dossier contenant tous les fichiers des formations

			while($nomElement = readdir($dossier)){ // pour chaque fichier
				if(strpos($nomElement, ".json") !== false){ // si c'est un .json
					/* on récupère les données du fichier */
					$contenuFichier = file_get_contents("./donnees/formations/".$nomElement);
					$donneesFichier = json_decode($contenuFichier, true);

					/* et on génère une carte retournable du carrousel avec le titre de la formation */
					echo('
						<div class="zoneDetectionSouris">
							<li class="element-carrousel">
								<div class="titre" onclick="redirigerVersPageInformationsDeLaFormation(this.querySelector(\'h2\').innerHTML)">
									<h2>'.$donneesFichier["nom"].'</h2>
								</div>
								<div class="descriptif">
									<span onclick="redirigerVersPageInformationsDeLaFormation(this.parentNode.previousElementSibling.querySelector(\'h2\').innerHTML)">Je découvre la formation</span>
								</div>
							</li>
						</div>

					');
				}
			}

			closedir($dossier);
		?>

	</ul>	
</div>

<script src="./formation/carrousel.js"></script>
