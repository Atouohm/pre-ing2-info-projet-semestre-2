Équipe "ALTF4" CY-TECH Pau (Pré-Ing2 2022/2023) :
	- GITTON Julien 
	- BIOUDI Mathéo
	- BARRERE Dorian
	- COURTHIAL Mathis
	- FAZILLE Tara


Merci de faire une demande à barreredorian332@gmail.com avant toute reproduction du site


Mode d'emploi du site "utilisateur" :

	- Page formation.php :
		C’est la page d’accueil du portail. L'utilisateur peut trouver des informations de contacts importants correspondant à un département précis. De plus, un carrousel des formations proposées par l'IFJR est exposé. L'utilisateur peut cliquer sur chaque carte pour accéder au descriptif détaillé de chaque formation. Il peut également rechercher directement une session de formation en utilisant directement le bouton prévu à cet effet.

	- Page informationsEssentielles.php :
		Les informations essentielles apparaissent les unes au-dessus des autres avec la possibilité d'accéder à l'information complète en cliquant sur le titre de celle-ci.

	- Page listeDesSessions.php :
		Sur la gauche, l'utilisateur peut renseigner différentes informations lui permettant de trier les sessions correspondantes à ses critères. Il n'est pas nécessaire de remplir tous les champs. Dans le bloc de droite, l'utilisateur peut voir les sessions triées selon ses critères et aller sur la page d'inscription en cliquant sur s'inscrire.

	- Page carte.php :
		Pour naviguer sur la carte interactive, il faut faire un clic gauche pour sélectionner une zone et un clic droit pour revenir en arrière ou alors utiliser le bouton prévu à cet effet. Les zones affichées en vert contiennent des sessions de formations alors que les zones en rouge n’en contiennent pas. En fonction de la zone sélectionnée sur la carte, les formations comprises dedans sont affichées sur la partie droite de la carte. L'utilisateur peut alors cliquer sur une formation pour accéder à sa fiche détaillée. Lorsque l'utilisateur sélectionne un département, les informations de contact du département (téléphone, adresse et mail) sont affichées.

	- Page connexion.php :
		Cette page permet aux administrateurs du site de se connecter en utilisant leur identifiant et mot de passe.


Mode d'emploi du site "administrateur" (page ./dashboard/dashboard.php) :

	- Onglet Créer une Formation :
		Cet onglet permet la création d'une nouvelle formation et la création de sa page associée. Ainsi, il est nécessaire de renseigner le nom, le format, le groupe et la durée de la formation. Il est aussi utile de décrire la formation grâce aux catégories et leurs parties pour aider à la compréhension. Ces données seront affichées dans la page associée à la formation. Un fichier pdf peut aussi être ajouté afin de décrire la formation.

	- Onglet Modifier une Formation :
		Dans la partie de gauche, il est possible de rechercher une formation par son titre. Il faut ensuite en sélectionner une. Sur la partie de droite, s'affichent alors toutes les informations de la formation. Le bouton poubelle permet de supprimer une formation, le bouton à sa gauche permet de dupliquer une formation. On retrouve ensuite la même interface que sur la page création d'une formation.

	- Onglet Modifier une Session :
		Dans un premier temps, il est nécessaire de sélectionner une formation dans la colonne de gauche (recherche par le nom de la formation possible). A droite, il est possible de créer une session associée à cette formation en utilisant le bouton correspondant. Il faudra fournir les informations demandées afin de valider l'ajout de la session. Il est aussi possible dans la colonne de droite de sélectionner une session déjà existante afin de la supprimer avec le bouton adéquat.

	- Onglet Modifier un Contact :
		Dans un premier temps, il faut sélectionner le département souhaité. Ensuite, les informations associées déjà enregistrées apparaissent et sont modifiables. Pour supprimer une information, il suffit de vider la case correspondante et de valider le formulaire.

	- Onglet Modifier les Informations Essentielles :
		Une information essentielle est composée d'un titre et d'un descriptif, tous les deux obligatoires. L'utilisation de la croix permet de supprimer une information essentielle et pour en ajouter une, il suffit d'utiliser le bouton créé à cet effet. Il y a au moins une information essentielle obligatoire.


Informations Supplémentaires :

	-  Le dossier "./proteger" doit impérativement être protégé. Suivant le type de serveur utilisé pour le site web (Apache/NGINX) il est nécessaire de le configurer afin de rendre ce dossier inaccessible car il contient des informations sensibles comme les identifiants des administrateurs.

	- Par défaut, le seul identifiant administrateur est : admin - admin. Pour le modifier ou en rajouter, il suffit de modifier le fichier "./proteger/connexion/identifiants.csv" et d'ajouter une ligne : "<identifiant>;<mot de passe>".

	- Pour toute question, contacter via l'adresse mail suivante : barreredorian332@gmail.com.