<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="./styleGlobal.css">
	<link rel="stylesheet" href="./navbarre/navbarre.css">
	<link rel="stylesheet" href="./footer/footer.css">
	<link rel="stylesheet" href="./informations/informations.css">
	<title>Informations - Formation</title>
</head>
<body>

	<?php
		include("./navbarre/navbarre.php");
	?>

	<main>
		<?php
			if(file_exists("./donnees/formations/".strtolower($_GET["formation"]).".json")){ // on vérifie que la formation demandée existe
				/* si c'est le cas on récupère toutes les données de la formation */
				$contenuFichier = file_get_contents("./donnees/formations/".strtolower($_GET["formation"]).".json");
				$donneesFichier = json_decode($contenuFichier, true);
		?>

		<!-- en tête de la formation contenant son titre et sa description rapide -->
		<div id="header-formation">
			<h2>Formation : <?php echo($donneesFichier["nom"]);?></h2>
			<pre><?php echo($donneesFichier["description"]);?></pre>
		</div>

		<!-- quelques information essentielles -->
		<div id="info-formation">
			<p><span class="bold">Format</span> : <?php echo($donneesFichier["format"]);?></p>
			<p><span class="bold">Groupe</span> :  <?php echo($donneesFichier["groupe"]);?></p>
			<p><span class="bold">Durée</span> : <?php echo($donneesFichier["duree"]);?></p>
			<?php // s'il y a un pdf associé on génère le bouton pour pouvoir le télécharger
				if($donneesFichier["pdfAssocie"] == "vrai"){
					$lienPdf = "./donnees/formations/".strtolower($donneesFichier["nom"]).".pdf";
					echo('<a class="boutonTelechargementPdf" href="'.$lienPdf.'" target="_blank">Télécharger le PDF</a>');
				}
			?>
		</div>

		<!-- boite principale de la page -->
		<div id="formation">

			<!-- boite contenant toutes les catégories et parties de description de la formation -->
			<section id="formation-gauche">

				<?php
					if(count($donneesFichier["informations"]) != 0){ // on regarde s'il y a des informations associées à la formation
						foreach($donneesFichier["informations"] as $clef => $valeur){
							if(preg_match('/^c[0-9]+[^p]/', $clef) != null){ // si c'est une catégorie
								if(strpos($clef, 'Titre') !== false){ // et que c'est un titre

									/* on génère un h2 */
									if(strpos($clef, 'c1Titre') !== false){
										echo('<h2 class="premierH2">'.$valeur.'</h2>');
									} else{
										echo('<h2>'.$valeur.'</h2>');
									}

								} else if(strpos($clef, 'Description') !== false && $valeur != null){ // sinon si c'est une description on génère un pre

									echo('<pre>'.$valeur.'</pre>');
								}

							} else if(preg_match('/^c[0-9]+p/', $clef) != null){ // même principe pour les parties des catégories
								if(strpos($clef, 'Titre') !== false){

									echo('<h3>'.$valeur.'</h3>');

								} else if(strpos($clef, 'Description') !== false && $valeur != null){

									echo('<pre>'.$valeur.'</pre>');
								}
								
							}
						}
					}
				?>

				<p id="derniereMiseAJour">Dernière mise à jour : <span style="color:red"><?php echo($donneesFichier["dateModification"]);?></span></p>
			</section>

			<section id="formation-droite">
				<h2 class="premierH2">Les Tarifs</h2>
					<p>L’IFJR met son expertise à disposition de la fédération France Victimes et de <a href="https://www.enap.justice.fr/contact">l’École Nationale d’Administration Pénitentiaire (ENAP)</a>.</p>
					<p>L’IFJR met également en place des formations avec certaines Directions Interrégionales des Services Pénitentiaires DISP et Directions Territoriales de la Protection Judiciaire de la Jeunesse DTPPJ. L’inscription à ces formations est généralement réservée aux agents de ces DISP et DTPJJ.</p>
					<p>Merci de vous rapprocher de ces structures qui vous informeront sur leurs tarifs et modalités d’accès.</p>
				<button onclick="redirigerVersPageListeSessions('<?php echo($donneesFichier["nom"]);?>')">Consultez les dates et villes</button> <!-- bouton qui redirige vers la page liste des sessions déjà triée suivant la formation actuelle -->
			</section>

		</div>

		<a href="#" id="revenir-en-haut-de-la-page">&#10506;</a>

		<?php
			} else{
				echo("<p id='formationPasTrouvee'>La formation que vous cherchez n'a pas été trouvée...</p>");
			}
		?>
	</main>

	<?php
		include("./footer/footer.html");
	?>

	<script type="text/javascript" src="./informations/informations.js"></script>
</body>
</html>
