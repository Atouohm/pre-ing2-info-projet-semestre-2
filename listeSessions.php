<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="./styleGlobal.css">
	<link rel="stylesheet" href="./navbarre/navbarre.css">
	<link rel="stylesheet" href="./footer/footer.css">
	<link rel="stylesheet" href="./listeSessions/listeSessions.css">
	<title>Liste des Sessions - Formation</title>
</head>
<body>

	<div id="notification"></div>

	<?php
		include("./navbarre/navbarre.php");
	?>

	<main>

		<!-- formulaire de tri -->
		<form id="formRecherche">
			<div class="categorie">
				<label>Type de formation :</label>
				<select class="selectNomFormation">
					<option value="0" selected>Toutes les Formations</option>
				</select>
			</div>

			<div class="categorie">
				<label>À partir de :</label>
				<input type="date" class="dateMinimale" name="dateMinimale" placeholder="ex: 00/00/0000" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}">
				<input type="time" class="heureMinimale" name="heureMinimale" placeholder="ex: 00:00" pattern="[0-9]{2}:[0-9]{2}">
			</div>

			<div class="categorie">
				<label>Jusqu'à :</label>
				<input type="date" class="dateMaximale" name="dateMaximale" placeholder="ex: 00/00/0000" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}">
				<input type="time" class="heureMaximale" name="heureMaximale" placeholder="ex: 00:00" pattern="[0-9]{2}:[0-9]{2}">
			</div>

			<div class="categorie">
				<label>Lieu de Session :</label>
				<input type="number" class="codePostal" name="codePostal" min="0" placeholder="Code Postal...">
				<input type="text" class="ville" name="ville" placeholder="Ville...">
			</div>

			<div class="boiteDesChoix">
				<button class="reset" type="reset" onclick="rechercherSessionsToutes()">Annuler</button>
				<button class="valider" type="button" onclick="filtrer(this.parentNode.parentNode)">Valider</button>
			</div>
		</form>

		<!-- boite contenant les résultats de la recherche -->
		<div class="boiteResultatsRechercheSessions">
			<h3>Voici la liste des Sessions répondant à vos critères</h3>
			<span class="ligneHorizontale"></span>
			<div class="listeResultatsSessions"></div>
		</div>
	</main>

	<?php
		include("./footer/footer.html");
	?>

	<script type="text/javascript" src="./listeSessions/listeSessions.js"></script>
</body>
</html>
