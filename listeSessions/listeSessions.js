/* les variables qui servent à plusieurs endroits */
const boiteListeDesSessions = document.querySelector(".boiteResultatsRechercheSessions .listeResultatsSessions");
const select = document.querySelector("#formRecherche select");
let tableauListeDesSessions;

lancerGenerationPage(); // on génère la page ie que l'on recherche les sessions, puis les formations, puis si il y a un critère (de formation) dans l'url on le prend en compte

async function lancerGenerationPage(){
	await rechercherSessionsToutes(); // on attend le temps de rechercher toutes les sessions
	await rechercherFormations(); // pareil pour les formations

	let nomFormationGet = recupererNomFormationGet(); // on récupère la variable get "formation"

	if(nomFormationGet != null && selectPossedeCetteOption(nomFormationGet.toLowerCase())){ // si elle est pas nulle et qu'elle est valide
		select.value = nomFormationGet.toLowerCase(); // on met à jour le select
		filtrer(document.getElementById("formRecherche")); // et on trie les sessions
	}
}

/* rechercher dans le select en fonction de la value des options */
function selectPossedeCetteOption(valueOptionRecherchee){
	let ok = false;
	select.querySelectorAll("option").forEach( option => {
		if(option.value == valueOptionRecherchee){
			ok = true;
		}
	});
	return ok;
}

/* fonction asynchrone qui recherche toutes les sessions */
async function rechercherSessionsToutes(nomFormation){

	const res = await fetch("./listeSessions/scriptSessionsRechercherToutes.php"); // on appelle le script

	if(res.ok){ // si la réponse et ok
		const resJSON = await res.json(); // on la "transforme" et objet js

		tableauListeDesSessions = resJSON; // on met à jour la variable globale tableauListeDesSessions
		creerListeSessions(tableauListeDesSessions); // on crée la liste de toutes les sessions
	} else{
		throw new Error("Erreur avec le script de recherche des sessions.");
	}
}

/* fonction asynchrone qui génère la liste de toutes les sessions */
function creerListeSessions(tableauSessions){
	boiteListeDesSessions.innerHTML = "";

	if(tableauSessions.length != 0){

		tableauSessions.forEach( session => {
			let itemSession = document.createElement("div");
			itemSession.setAttribute("class", "itemSession");

			let dateSession = new Date(session.date);

			itemSession.innerHTML = `
				<span style="font-weight: bold; font-size: 18px;">${session.nomFormationAssociee}</span>

				<span class="ligneHorizontale mini"></span>

				<span style="font-weight: bold;">${dateSession.toLocaleDateString()}</span>

				<span class="ligneHorizontale mini"></span>

				<span>${session.horaire}</span>

				<span class="ligneHorizontale mini"></span>

				<span style="font-weight: bold;">${session.ville}</span>

				<span class="ligneHorizontale mini"></span>

				<span>${session.adresse}</span>

				<span class="ligneHorizontale mini"></span>

				<span>${session.codePostal}</span>

				<span class="ligneHorizontale mini"></span>

				<a class="lienInscription" href="${session.lienInscription}" target="_blank">S'inscrire</a>
			`

			boiteListeDesSessions.appendChild(itemSession);
		});

	} else{
		let texteAucuneSession = document.createElement("p");
		texteAucuneSession.setAttribute("class", "texteAucuneSession");
		texteAucuneSession.innerHTML = "Il n'y a aucune sessions avec les critères actuels."
		boiteListeDesSessions.appendChild(texteAucuneSession);
	}
}

/* fonction asynchrone qui recherche toutes les toute les formations */
async function rechercherFormations(){
	const res = await fetch("./dashboard/scriptFormationRechercher.php");

	if(res.ok){
		const resJSON = await res.json();
		tableauResultatsRechercheFormations = resJSON;
		creerListeOptionsFormations(tableauResultatsRechercheFormations);
	} else{
		throw new Error("Erreur avec le script de recherche des formations.");
	}
}

/* fonction asynchrone qui génère les options du select des formations */
function creerListeOptionsFormations(tableauFormations){	
	tableauFormations.forEach( formation => {
		let optionFormation = document.createElement("option");
		optionFormation.setAttribute("value", formation.nom.toLowerCase());
		optionFormation.innerHTML = formation.nom;

		select.appendChild(optionFormation);
	});
}

/* fonction qui filtre la liste des sessions quand le formulaire envoyé */
function filtrer(formulaire){
	if(donneesTemporellesFormulaireSontValides(formulaire)){ // si les données temporelles du formulaire sont valides
		let tableauListeDesSessionsFiltre; // on créer le tableau des liste des sessions filtrées

		/* on trie le tableau de la liste de toutes les sessions tableauListeDesSessions pour chaque critère. Le résultat est contenu dans le tableau tableauListeDesSessionsFiltre */
		tableauListeDesSessionsFiltre = filtrerNomFormation(formulaire.querySelector(".selectNomFormation").options[formulaire.querySelector(".selectNomFormation").selectedIndex].value, tableauListeDesSessions);
		tableauListeDesSessionsFiltre = filtrerDateMinimale(formulaire.querySelector(".dateMinimale").value, tableauListeDesSessionsFiltre);
		tableauListeDesSessionsFiltre = filtrerHeureMinimale(formulaire.querySelector(".heureMinimale").value, tableauListeDesSessionsFiltre);
		tableauListeDesSessionsFiltre = filtrerDateMaximale(formulaire.querySelector(".dateMaximale").value, tableauListeDesSessionsFiltre);
		tableauListeDesSessionsFiltre = filtrerHeureMaximale(formulaire.querySelector(".heureMaximale").value, tableauListeDesSessionsFiltre);
		tableauListeDesSessionsFiltre = filtrerCodePostal(formulaire.querySelector(".codePostal").value, tableauListeDesSessionsFiltre);
		tableauListeDesSessionsFiltre = filtrerVille(formulaire.querySelector(".ville").value, tableauListeDesSessionsFiltre);

		creerListeSessions(tableauListeDesSessionsFiltre);
	} else{
		afficherNotification("La critères temporels ne sont pas valides", "pasOk"); // si c'est pas ok fait une notification pour le dire
	}
	
}

/* fonctions qui permette de filtrer suivant chaque critère du formulaire */

function filtrerNomFormation(nomFormation, tableauSessions){
	if(nomFormation != "0"){
		return tableauSessions.filter( session => {
			return session.nomFormationAssociee == nomFormation;
		});
	} else{
		return tableauSessions;
	}
}

function filtrerDateMinimale(dateMinimale, tableauSessions){
	if(dateMinimale != ""){
		dateMinimale = new Date(dateMinimale);

		return tableauSessions.filter( session => {
			let sessionDate = new Date(session.date);
			return sessionDate >= dateMinimale;
		});
	} else{
		return tableauSessions;
	}
}

function filtrerHeureMinimale(heureMinimale, tableauSessions){
	if(heureMinimale != ""){
		return tableauSessions.filter( session => {
			return heureAvantOuEgale(heureMinimale, session.horaire);
		});
	} else{
		return tableauSessions;
	}
}

function filtrerDateMaximale(dateMaximale, tableauSessions){
	if(dateMaximale != ""){
		dateMaximale = new Date(dateMaximale);

		return tableauSessions.filter( session => {
			let sessionDate = new Date(session.date);
			return sessionDate <= dateMaximale;
		});
	} else{
		return tableauSessions;
	}
}

function filtrerHeureMaximale(heureMaximale, tableauSessions){
	if(heureMaximale != ""){
		return tableauSessions.filter( session => {
			return heureAvantOuEgale(session.horaire, heureMaximale);
		});
	} else{
		return tableauSessions;
	}
}

function filtrerCodePostal(codePostal, tableauSessions){
	if(codePostal != ""){
		return tableauSessions.filter( session => {
			return session.codePostal == codePostal;
		});
	} else{
		return tableauSessions;
	}
}

function filtrerVille(ville, tableauSessions){
	if(ville != ""){
		return tableauSessions.filter( session => {
			return session.ville.toLowerCase().replace(/\s/g, "").includes(ville.toLowerCase().replace(/\s/g, ""));
		});
	} else{
		return tableauSessions;
	}
}

function heureAvantOuEgale(heure1, heure2){
	let tableauHeure1 = heure1.split(/[:;h]/);
	let tableauHeure2 = heure2.split(/[:;h]/);

	return tableauHeure1[0]<tableauHeure2[0] || (tableauHeure1[0]==tableauHeure2[0] && tableauHeure1[1]<=tableauHeure2[1]);
}

/* fonctions qui vérifie si la date minimale et avant la date maximale (ou égale) */
function donneesTemporellesFormulaireSontValides(formulaire){
	let sontOkDates = true;
	let sontOkHeures = true;

	if(formulaire.querySelector(".dateMinimale").value != "" && formulaire.querySelector(".dateMaximale").value != ""){
		let dateMinimale = new Date(formulaire.querySelector(".dateMinimale").value);
		let dateMaximale = new Date(formulaire.querySelector(".dateMaximale").value);
		sontOkDates = dateMinimale <= dateMaximale;
	}

	if(formulaire.querySelector(".heureMinimale").value != "" && formulaire.querySelector(".heureMaximale").value != ""){
		sontOkHeures = heureAvantOuEgale(formulaire.querySelector(".heureMinimale").value, formulaire.querySelector(".heureMaximale").value);
	}
	
	return sontOkDates && sontOkHeures;
}

/* fonctions qui automatise l'affichage d'une notification en changeant la classe de la boite de notification puis rejouant une animation avec requestAnimationFrame */
function afficherNotification(message, type){
	let notification = document.getElementById("notification");
	notification.innerHTML = message;

	notification.removeAttribute("class");
	window.requestAnimationFrame( () => {
		window.requestAnimationFrame( () => {
			notification.setAttribute("class", "afficher" + " " + type);
		});
	});
}

/* fonction qui lit l'url et qui récupère si elle existe la valeur de la variable formation */
function recupererNomFormationGet(){
	let variablesGet = window.location.search.slice(1,window.location.search.length).split("&");
	let nomFormationGet = null;

	for(let i=0 ; i<variablesGet.length ; i++){
		let variableGet = variablesGet[i].split("=");
		if(variableGet[0] == "formation"){
			nomFormationGet = decodeURIComponent(variableGet[1]);
		}
	}

	return nomFormationGet;
}