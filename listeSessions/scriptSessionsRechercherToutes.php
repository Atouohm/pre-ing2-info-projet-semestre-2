<?php
	$lienDossier = '../donnees/formations/';
	$dossier = opendir($lienDossier); // on ouvre le dossier des formations

	$tableauDonnees = [];

	while($nomElement = readdir($dossier)){ // pour tout les fichiers
		if(strpos($nomElement, ".json") !== false){ // si c'est un json

			/* on récupère les données du fichier */
			$contenuFichier = file_get_contents("../donnees/formations/".$nomElement);
			$donneesFichier = json_decode($contenuFichier, true);

			for($i=0 ; $i<count($donneesFichier["sessions"]) ; $i++){ // on parcourt toutes les sessions de la formation
				$donneesFichier["sessions"][$i]["nomFormationAssociee"] = $donneesFichier["nom"]; // et on rajoute à la session l'info de à quelle formation elle appartient
			}
			
			$tableauDonnees = array_merge($tableauDonnees, $donneesFichier["sessions"]); // on rajoute les sessions à la liste qu'on avait déjà
		}
	}

	echo(json_encode($tableauDonnees)); // on encode et on envoie

	closedir($dossier);
?>
