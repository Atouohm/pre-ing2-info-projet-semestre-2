/* fonction qui rabat le déroulant */
function cacherContenu(contenu) {

	/* à ce moment là il n'y a pas de style html et le style html est réglé sur auto. Le problème est que le css désactive intentionnellement les animations/transition quand la tailles sont en auto. */
	let contenuHeight = contenu.scrollHeight;
	contenu.style.height = contenuHeight + 'px'; // donc on définit un style html (qui est prioritaire sur le style css) avec une valeur de hauteur définie en pixels (la même que auto en fait mais au moins elle est définie en pixels)

	/* on attend actualise le style de la page */
	window.requestAnimationFrame(function() {
		window.requestAnimationFrame(function() {
			contenu.parentNode.classList.remove("ouvert"); // on dit que le déroulant est fermé (puis la à la ligne suivant on va faire la transition et juste après la transition le style html va être supprimé donc il faut se préparer en mettant le style css à 0)
			contenu.style.height = 0; // et juste après on met la taille à 0 donc le transition se fait (et d'ailleurs après cette transition le event listener transitionend enlève toute seul l'attribut style html juste après la transition)
		});
	});

}

/* fonction qui ouvre le déroulant */
function afficherContenu(contenu) {
	let contenuHeight = contenu.scrollHeight;
	contenu.parentNode.classList.add("ouvert");
	contenu.style.height = contenuHeight + 'px'; // on met le style html de hauteur à la taille nécessaire pour ne pas qu'il n'y est de scroll
}

/* pour chaque déroulant on ajoute à la partie en tête un event listenener qui suivant si le déroulant est déjà ouvert ou non le cache ou l'ouvre */
document.querySelectorAll(".enTete").forEach( enTete => {
	enTete.addEventListener("click", event => {
		let contenu = enTete.nextElementSibling;
		let estOuvert = enTete.parentNode.classList.contains("ouvert");
			
		if(estOuvert){
			cacherContenu(contenu);
		} else {
			afficherContenu(contenu);
		}
	});
});

document.querySelectorAll(".contenu").forEach( contenu => {
	contenu.addEventListener('transitionend', function(e) { // une fois la transition finie
		contenu.removeAttribute("style"); // on enlève l'attribut style donc la taille 
	});
});